# README #

# Установка веб сервера  #

Перед началом процесса, убедитесь что у вас установлена последняя версия [Python](https://www.python.org/download/releases/3.0/).

1. Склонируйте приложение себе на локальную машину.
2. Перейдите в папку с приложением, в его корень, запустите cmd.
3. Выполните следующую команду в командной строке, чтобы создать виртуальное окружение C:\Python34\python -m venv venv.
4. Запустите виртуальное окружение командой venv\Scripts\activate
5. Обновите pip командой python -m pip install --upgrade pip
6. Установите django командой pip install django==1.10.3
7. Установите следующие фраемворки для работы сервера
     * pip install djangorestframework
     * pip install djoser
     * pip install Pillow
     * pip install pusher
     * pip install reportlab
7. Запустите сервер командой <Путь до python>\python manage.py runserver

Для работы под группой "Клиент", необходимо просто зарегестрироваться
Для работы под другими группами ("Консультант", "Ученый"), необходимо зайти под группой "Администраторы"
Доступы: email: admin@admin.com, password: admin

Создать пользователя, после, при редактировании, выбрать ему группу.