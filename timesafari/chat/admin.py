from django.contrib import admin
from .models import Message

admin.site.register(
    Message,
    list_display = ["id", "sender", "receiver"],
)