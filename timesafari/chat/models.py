from django.db import models
from django.db.models import Q
from timesafari.user.models import TimeSafariUser

class Message(models.Model):
    content = models.TextField()
    sender = models.ForeignKey(TimeSafariUser, related_name='message_sender', null=True, blank=True)
    seen = models.BooleanField(default=False)
    receiver = models.ForeignKey(TimeSafariUser, related_name='message_receiver', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def last_messages(user1_id, user2_id, offset, cnt = 10):
        messages = Message.objects.all().filter(Q(sender__id = user1_id, receiver__id = user2_id) | Q(sender__id = user2_id, receiver__id = user1_id)).order_by('-id')[offset:offset+cnt]
        content = list()
        for message in messages:
            content.append({"sender_id": message.sender.id, "content": message.content})
        return content

    @staticmethod
    def unseen_list(user_id):
        messages = Message.objects.all().filter(receiver__id = user_id, seen=False)
        ret = set()
        for message in messages:
            ret.add(message.sender.id)
        return list(ret)

    @staticmethod
    def to_seen(sender_id, receiver_id):
        messages = Message.objects.all().filter(sender__id = sender_id, receiver__id = receiver_id, seen=False)
        for message in messages:
            message.seen = True
            message.save()
        