from django.shortcuts import render
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.template import RequestContext
from datetime import datetime
from timesafari.user.models import TimeSafariUser
from django.contrib.auth.decorators import login_required
from pusher import Pusher
from .models import Message
import json

@login_required(login_url='/login/')
def chat_index(request):
    users = TimeSafariUser.objects.all()
    return render(request, 'chat.html', {'users': users, 'current_user_id': request.user.id})

def chat_auth(request):
    if request.user.is_authenticated:
        socket_id = request.POST['socket_id']
        channel_name = request.POST['channel_name']
        
        user_id = request.user.id
        
        if "private" in channel_name and str(user_id) not in channel_name:
            return HttpResponse(status=403)
        else:
            pusher = Pusher(app_id = "274997", secret = "9181ec6986525327ec50", key = "e9f4c44c23228e97eb04", cluster = "eu")
            return JsonResponse(pusher.authenticate(
                channel = channel_name,
                socket_id = socket_id,
                custom_data={
                    'user_id': request.user.id,
                    'user_email': request.user.email
                }
            ))
    else:
        return HttpResponse(status=403)

def chat_send(request):
    pusher = Pusher(app_id = "274997", secret = "9181ec6986525327ec50", key = "e9f4c44c23228e97eb04", cluster = "eu")
    sender_id = request.POST['sender_id']
    receiver_id = request.POST['receiver_id']
    message_channel = 'private-chat-' + str(min(sender_id, receiver_id)) + '-' + str(max(sender_id, receiver_id))
    message = request.POST['message']
    
    pusher.trigger(message_channel, 'new_message', {'message': message, 'sender_id': sender_id})
    # Save message to DB
    sender = TimeSafariUser.objects.get(pk=sender_id)
    receiver = TimeSafariUser.objects.get(pk=receiver_id)
    Message.objects.create(content = message, sender=sender, receiver=receiver)

    receiver_notification_channel = 'private-notifications-' + str(receiver_id)
    pusher.trigger(receiver_notification_channel, 'new_message', {'sender_id': sender_id, 'content': message, 'receiver_id': receiver_id})
    return HttpResponse(json.dumps({'success': True}), content_type='application/json')

def chat_join(request):
    sender_id = request.POST['sender_id']
    receiver_id = request.POST['receiver_id']
    offset = int(request.POST['offset'])

    last_message = Message.last_messages(sender_id, receiver_id, offset)
    return HttpResponse(json.dumps({'messages': last_message}), content_type='application/json')

def chat_unseen(request):
    user_id = request.user.id
    unseen = Message.unseen_list(user_id)
    return HttpResponse(json.dumps({'id': unseen}), content_type='applications/json')

def chat_seen(request):
    user_id = request.user.id
    sender_id = request.POST['sender_id']
    Message.to_seen(sender_id, user_id)
    return HttpResponse(json.dumps({'detail': 'succeed'}), content_type='applications/json')
