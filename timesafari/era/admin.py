from django.contrib import admin
from .models import Era

# Register your models here.
admin.site.register(Era)
