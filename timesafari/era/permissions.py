from rest_framework import permissions

class EraPermission(permissions.BasePermission):
    def has_permission(self, request, view):
    	if not request.user.is_authenticated:
    		return False
    	else:
            return 'era.add_era' in request.user.get_all_permissions()
	    	#return request.user.has_perm('tour.add_tour')

 