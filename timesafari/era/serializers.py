from rest_framework import serializers
from .models import Era

class EraSerializer(serializers.ModelSerializer):
	class Meta:
		model = Era
		fields = ('id', 'name', 'price', 'picture', 'description', 'created', 'updated', )

class EraDisplaySerializer(serializers.ModelSerializer):
	class Meta:
		model = Era
		fields = ('id', 'name', )
