from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from rest_framework.authtoken.models import Token
from timesafari.user.models import TimeSafariUser
from .models import Era
from django.core.exceptions import ObjectDoesNotExist

from django.core.files.images import ImageFile
import tempfile
from django.test.utils import override_settings

def get_test_image_file():
    file = tempfile.NamedTemporaryFile(suffix='.jpg')
    image = ImageFile(file, name=file.name)
    return image

MEDIA_ROOT = tempfile.mkdtemp()

@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class CreateEraTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('era-create')
		
	def test_create_era_without_login(self):
		data = {'name': 'normal era', 'price': 300}
		response = self.guest.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_create_era_without_permission(self):
		data = {'name': 'normal era', 'price': 300}
		response = self.client.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

	def test_create_era_with_empty_name(self):
		data = {'name': '', 'price': 300}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_create_era_with_empty_price(self):
		data = {'name': 'new era'}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Era.objects.all()), 1)
		era = Era.objects.get(id=1)
		self.assertEqual(era.name, 'new era')
		self.assertEqual(era.price, 0)

	def test_create_era_with_negative_price(self):
		data = {'name': 'new era', 'price':-10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_create_era_with_picture(self):
		#picture = get_test_image_file()
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		data = {'name': 'new era', 'price': 10, 'picture': picture}
		response = self.admin.post(self.url, data=data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_era_with_description(self):
		data = {'name': 'new era', 'price': 10, 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.'}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_era_with_all_fields(self):
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		data = {'name': 'new era', 'price': 10, 'picture': picture, 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.'}
		response = self.admin.post(self.url, data=data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class GetEraListTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('era-list')

		for i in range(10):
			self.create_new_era()

	def create_new_era(self):
		Era.objects.create(name='new era', price=100)

	def test_get_all_eras_without_login(self):
		response = self.guest.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 10)		

	def test_get_all_eras_with_client(self):
		response = self.client.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 10)

	def test_get_all_eras_with_admin(self):
		response = self.admin.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 10)


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class GetEraDetailTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('era-detail', args = [2])

		for i in range(10):
			self.create_new_era('era ' + str(i))

	def create_new_era(self, name):
		Era.objects.create(name=name, price=100)

	def test_get_era_without_login(self):
		response = self.guest.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 7)
		self.assertEqual(response.data['id'], 2)
		self.assertEqual(response.data['name'], 'era 1')

	def test_get_era_with_client(self):
		response = self.client.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 7)
		self.assertEqual(response.data['id'], 2)
		self.assertEqual(response.data['name'], 'era 1')

	def test_get_era_with_admin(self):
		response = self.admin.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 7)
		self.assertEqual(response.data['id'], 2)
		self.assertEqual(response.data['name'], 'era 1')

	def test_get_none_exist_era(self):
		url = reverse('era-detail', args = [200])
		response = self.admin.get(url)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)



@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class EditEraTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('era-edit', args = [2])

		for i in range(10):
			self.create_new_era('era ' + str(i))		
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		self.data = {'name': 'new era', 'price': 10, 'picture': picture}

	def create_new_era(self, name):
		Era.objects.create(name=name, price=100, description='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.',
			picture='MonolophosaurusHiRes_usl6ti.jpg')

	def test_edit_era_without_login(self):
		response = self.guest.put(self.url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
		era = Era.objects.get(id=2)
		self.assertEqual(era.name, 'era 1')

	def test_edit_era_with_client(self):
		response = self.client.put(self.url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
		era = Era.objects.get(id=2)
		self.assertEqual(era.name, 'era 1')

	def test_edit_era_with_admin(self):
		response = self.admin.put(self.url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		era = Era.objects.get(id=2)
		self.assertEqual(era.name, 'new era')

	def test_edit_none_exist_era(self):
		url = reverse('era-edit', args = [200])
		response = self.admin.put(url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

	def test_edit_era_with_empty_name(self):
		data = {'name': '', 'price': 300}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_era_with_empty_price(self):
		data = {'name': 'new era'}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		era = Era.objects.get(id=2)
		self.assertEqual(era.name, 'new era')
		self.assertEqual(era.price, 100)

	def test_edit_era_with_negative_price(self):
		data = {'name': 'new era', 'price':-10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_era_with_picture(self):
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		data = {'name': 'new era', 'price': 10, 'picture': picture}
		response = self.admin.put(self.url, data=data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		era = Era.objects.get(id=2)
		self.assertEqual(era.name, 'new era')
		self.assertEqual(era.price, 10)

	def test_create_era_with_description(self):
		data = {'name': 'new era', 'price': 10, 'description': 'Nam volutpat nec nisi sed euismod.'}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		era = Era.objects.get(id=2)
		self.assertEqual(era.name, 'new era')
		self.assertEqual(era.price, 10)
		self.assertEqual(era.description, 'Nam volutpat nec nisi sed euismod.')

	def test_create_era_with_all_fields(self):
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		data = {'name': 'new era', 'price': 10, 'picture': picture, 'description': 'Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.'}
		response = self.admin.put(self.url, data=data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		era = Era.objects.get(id=2)
		self.assertEqual(era.name, 'new era')
		self.assertEqual(era.price, 10)
		self.assertEqual(era.description, 'Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.')



@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class GetEraDeleteTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('era-delete', args = [2])

		for i in range(10):
			self.create_new_era('era ' + str(i))

	def create_new_era(self, name):
		Era.objects.create(name=name, price=100)

	def test_delete_era_without_login(self):
		response = self.guest.delete(self.url)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
		era = Era.objects.get(id=2)
		self.assertEqual(era.name, 'era 1')
		self.assertEqual(era.price, 100)

	def test_delete_era_with_client(self):
		response = self.client.delete(self.url)
		self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
		era = Era.objects.get(id=2)
		self.assertEqual(era.name, 'era 1')
		self.assertEqual(era.price, 100)

	def test_delete_era_with_admin(self):
		response = self.admin.delete(self.url)
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
		try:
			era = Era.objects.get(id=2)
		except ObjectDoesNotExist:
			pass
		else:
			assert False, "Object was not deleted!"

	def test_delete_none_exist_era(self):
		url = reverse('era-delete', args = [200])
		response = self.admin.delete(url)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
	