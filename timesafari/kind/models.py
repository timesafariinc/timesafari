from django.db import models
from timesafari.era.models import Era
from django.core.validators import MinValueValidator

class Kind(models.Model):
	name = models.CharField(max_length=250)
	era = models.ForeignKey(Era, on_delete=models.PROTECT)
	DANGER_LEVELS = (
		(0, 'Low'),
		(1, 'Medium'),
		(2, 'High'),
		(3, 'Very High'),
	)
	danger = models.IntegerField(choices=DANGER_LEVELS)
	description = models.TextField(blank=True)
	picture = models.FileField(blank=True)
	price = models.PositiveIntegerField(default=0,
										validators=[MinValueValidator(0)])
	count = models.PositiveIntegerField(default=0,
										validators=[MinValueValidator(0)])
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	
	def __str__(self):
		return self.name
