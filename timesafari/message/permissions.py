from rest_framework import permissions

class MessageViewPermission(permissions.BasePermission):
	def has_permission(self, request, view):
		user = request.user
		return user.is_authenticated

class MessageCreatePermission(permissions.BasePermission):
    def has_permission(self, request, view):
    	user = request.user
    	if not user.is_authenticated:
    		return False
    	return 'message.add_message' in user.get_all_permissions()

class MessageUpdatePermission(permissions.BasePermission):
	def has_permission(self, request, view):
		user = request.user
		if not user.is_authenticated:
			return False
		return 'message.change_message' in user.get_all_permissions()
 
class MessageDeletePermission(permissions.BasePermission):
	def has_permission(self, request, view):
		user = request.user
		if not user.is_authenticated:
			return False
		if 'message.delete_message' not in user.get_all_permissions():
			return False

		return True