from rest_framework import serializers
from .models import Message
from timesafari.user.serializers import UserDisplayInfoSerializer

class MessageSerializer(serializers.ModelSerializer):
	sender = UserDisplayInfoSerializer()
	receiver = UserDisplayInfoSerializer()
	class Meta:
		model = Message
		fields = ('id', 'sender', 'receiver', 'content',
			'created', 'updated', )

class MessageCreateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Message
		exclude = ('sender', )
