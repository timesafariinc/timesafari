from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from rest_framework.authtoken.models import Token
from timesafari.user.models import TimeSafariUser
from .models import Message
from django.core.exceptions import ObjectDoesNotExist

from django.core.files.images import ImageFile
import tempfile
from django.test.utils import override_settings

def get_test_image_file():
    file = tempfile.NamedTemporaryFile(suffix='.jpg')
    image = ImageFile(file, name=file.name)
    return image

MEDIA_ROOT = tempfile.mkdtemp()

@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class CreateMessageTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('message-create')
		
	def test_create_message_without_login(self):
		data = {'content': 'hello', 'receiver': 1}
		response = self.guest.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_create_message_with_client(self):
		data = {'content': 'hello', 'receiver': 1}
		response = self.client.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_message_with_empty_receiver(self):
		data = {'content': 'hello'}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_create_message_with_empty_content(self):
		data = {'receiver': 1}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_create_message_with_non_exist_receiver(self):
		data = {'content': 'Hello', 'receiver': 100}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class GetMessageListTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('message-list')

		for i in range(10):
			Message.objects.create(content='hello', sender=TimeSafariUser.objects.get(id=1),
								receiver=TimeSafariUser.objects.get(id=2))
		for i in range(10):
			Message.objects.create(content='hello', sender=TimeSafariUser.objects.get(id=2),
								receiver=TimeSafariUser.objects.get(id=1))

	def test_get_messages_without_login(self):
		response = self.guest.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_get_messages_with_client(self):
		response = self.client.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 20)

	def test_get_messages_with_admin(self):
		response = self.admin.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 20)

@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class GetMessageDetailTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		TimeSafariUser.objects.create_user('client2@client.com', 'client2', 'client2', 'client2')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)
		data = {'email': 'client2@client.com', 'password': 'client2'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.client2 = APIClient()
		token = Token.objects.get(user__email='client2@client.com')
		self.client2.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('message-detail', args = [2])

		for i in range(10):
			Message.objects.create(content='hello', sender=TimeSafariUser.objects.get(id=1),
								receiver=TimeSafariUser.objects.get(id=2))
		for i in range(10):
			Message.objects.create(content='hello', sender=TimeSafariUser.objects.get(id=2),
								receiver=TimeSafariUser.objects.get(id=1))
		for i in range(10):
			Message.objects.create(content='hello', sender=TimeSafariUser.objects.get(id=3),
								receiver=TimeSafariUser.objects.get(id=1))

	def test_get_message_without_login(self):
		response = self.guest.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_get_message_with_client(self):
		response = self.client.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_message_with_admin(self):
		response = self.admin.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_none_exist_message(self):
		url = reverse('message-detail', args = [200])
		response = self.admin.get(url)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

	def test_get_not_own_message(self):
		response = self.client2.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

