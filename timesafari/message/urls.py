from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
	# /message/
	url(r'^$', views.MessageListView.as_view(), name='message-list'),
	# /message/get
	url(r'^get/$', views.MessageListView.as_view(), name='message-list'),
	# /message/<pk>/
	url(r'^(?P<pk>[0-9]+)/$', views.MessageRetrieveView.as_view(), name='message-detail'),
	# /message/get/<pk>/
	url(r'^get/(?P<pk>[0-9]+)/$', views.MessageRetrieveView.as_view(), name='message-detail'),
	# /message/delete/<pk>/
	url(r'^delete/(?P<pk>[0-9]+)/$', views.MessageDeleteView.as_view(), name='message-delete'),
	# /message/add/
	url(r'^add/$', views.MessageCreateView.as_view(), name='message-create'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
