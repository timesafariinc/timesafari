from rest_framework import permissions
from .models import Review

class ReviewPermission(permissions.BasePermission):
    def has_permission(self, request, view):
    	if not request.user.is_authenticated:
    		return False
    	else:
            return 'review.delete_review' in request.user.get_all_permissions()
	    	#return request.user.has_perm('tour.add_tour')

class ReviewUpdatePermission(permissions.BasePermission):
	def has_permission(self, request, view):
		if not request.user.is_authenticated:
			return False
		id = int(view.kwargs['pk'])
		if not Review.objects.filter(pk=id).exists():
			return False
		
		review = Review.objects.get(pk = id)
		if review.client == request.user:
			return True
		return 'review.change_review' in request.user.get_all_permissions()
		