from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
	# /review/
	url(r'^$', views.ReviewListView.as_view(), name='review-list'),
	# /review/get/
	url(r'^get/$', views.ReviewListView.as_view(), name='review-list'),
	# /review/<pk>/
	url(r'^(?P<pk>[0-9]+)/$', views.ReviewRetrieveView.as_view(), name='review-detail'),
	# /review/get/<pk>/
	url(r'^get/(?P<pk>[0-9]+)/$', views.ReviewRetrieveView.as_view(), name='review-detail'),
	# /review/add/
	url(r'^add/$', views.ReviewCreateView.as_view(), name='review-create'),
	# /review/edit/
	url(r'^edit/(?P<pk>[0-9]+)/$', views.ReviewUpdateView.as_view(), name='review-edit'),
	# /review/delete/<pk>/
	url(r'^delete/(?P<pk>[0-9]+)/$', views.ReviewDeleteView.as_view(), name='review-delete'),
]

urlpatterns = format_suffix_patterns(urlpatterns)