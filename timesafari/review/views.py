from rest_framework import status, generics
from .models import Review
from .serializers import ReviewSerializer, ReviewCreateSerializer
from rest_framework import permissions
from .permissions import ReviewPermission, ReviewUpdatePermission

class ReviewCreateView(generics.CreateAPIView):
    serializer_class = ReviewCreateSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def perform_create(self, serializer):
        serializer.save(client=self.request.user)

class ReviewListView(generics.ListAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    permission_classes = (permissions.AllowAny, )

    def get_queryset(self):
        queryset = Review.objects.all()
        request_tour = self.request.query_params.get('tour', None)
        if request_tour is not None:
            queryset = queryset.filter(tour=request_tour)
        request_client = self.request.query_params.get('client', None)
        if request_client is not None:
            queryset = queryset.filter(client=request_client)
        return queryset

class ReviewUpdateView(generics.UpdateAPIView):
	serializer_class = ReviewCreateSerializer
	queryset = Review.objects.all()
	permission_classes = (ReviewUpdatePermission, )    
        
class ReviewRetrieveView(generics.RetrieveAPIView):
	serializer_class = ReviewSerializer
	queryset = Review.objects.all()
	permission_classes = (permissions.AllowAny, )

class ReviewDeleteView(generics.DestroyAPIView):
	serializer_class = ReviewSerializer
	queryset = Review.objects.all()
	permission_classes = (ReviewPermission, )
