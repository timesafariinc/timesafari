define([
    'jquery',
    'jquery.cookie',
    'underscore',
    'backbone',
    'backbone-validate',
    'bootstrap',
    './app/collections/panel_items'
], function ($, cookie, _, Backbone, Validate, Bootstrap, Collections) {
    var status = {
        'waiting'       : 0,
        'processing'    : 1,
        'finish'        : 2,
        'passed'        : 3
    };
    if($.cookie('auth')) {
        $.ajaxSetup({
            headers: {'Authorization': 'Token ' + $.cookie('auth')}
        });
    }
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
            }
        }
    });
    app = {
        settings: {
            client: {
                panels: [{
                    icon        : '',
                    id          : 'tour.client',
                    name        : 'Заявки в очереди',
                    fields      : ['name', 'event_date', 'status'],
                    action      : 'tour/:id',
                    addAction   : 'tour/create',
                    collection  : new Collections['tour'](),
                    filter      : function (model) {
                        return model.get('status') == 0 || model.get('status') == 1;
                    }
                }, {
                    icon        : '',
                    id          : 'tour.client',
                    name        : 'История заявок',
                    fields      : ['name', 'event_date', 'status'],
                    action      : 'tour/:id',
                    addAction   : '',
                    collection  : new Collections['tour'](),
                    filter      : function (model,a) {
                        return model.get('status') == 2 || model.get('status') == 3;
                    }
                }, {
                    icon        : '',
                    id          : 'tour.passed',
                    name        : 'Оповещения',
                    fields      : ['sender', 'updated', 'content'],
                    action      : 'tour/passed',
                    addAction   : '',
                    collection  : new Collections['message']()
                }],
                chat: {
                    beginFilter: 'group=3'
                }
            },
            consultant: {
                panels: [{
                    icon        : '',
                    id          : 'tour.new',
                    name        : 'НОВЫЕ ЗАЯВКИ',
                    fields      : ['name', 'event_date', 'client'],
                    action      : 'tour/:id',
                    addAction   : '',
                    collection  : new Collections['tour']('paid=0&status=' + status['waiting'])
                }, {
                    icon        : '',
                    id          : 'tour.approved',
                    name        : 'ОДОБРЕННЫЕ ЗАЯВКИ',
                    fields      : ['name', 'event_date', 'client'],
                    action      : 'tour/:id',
                    addAction   : '',
                    collection  : new Collections['tour']('paid=0&status=' + status['processing'])
                }, {
                    icon        : '',
                    id          : 'tour.paid',
                    name        : 'ОПЛАЧЕННЫЕ ЗАЯВКИ',
                    fields      : ['name', 'event_date', 'client'],
                    action      : 'tour:id',
                    addAction   : '',
                    collection  : new Collections['tour']('paid=1'),
                    filter      : function (model,a) {
                        return model.get('status') != 2;
                    }
                }]
            },
            scientist: {
                panels: [{
                    icon        : '',
                    id          : 'era',
                    name        : 'СПИСОК ПЕРИОДОВ',
                    fields      : ['name', 'description'],
                    action      : 'era/:id',
                    addAction   : 'era/create',
                    collection  : new Collections['era']()
                }, {
                    icon        : '',
                    id          : 'kind',
                    name        : 'СПИСОК ВИДОВ',
                    fields      : ['name', 'era', 'danger'],
                    action      : 'kind/:id',
                    addAction   : 'kind/create',
                    collection  : new Collections['kind']()
                }]
            },
            admin: {}
        }
    };

    return app;
});