define([
    '../models/kind'
], function (Model) {
    return Backbone.Collection.extend({

        url : '/kind/',

        model: Model

    })
});