define([
    '../models/message'
], function (Model) {
    return Backbone.Collection.extend({

        url: 'chat/join/',

        parse: function(response) {
            return response.messages ? response.messages : [];
        },

        model: Model,

        create: function (attr, options) {
            var parent = this;
            $.ajax({
                type: "POST",
                url: '/chat/send/',
                dataType: "json",
                data: attr,
                success: function (msg) {
                    var model = new Model(attr);
                    parent.add(model);
                    if(options && options['success']) options['success'](msg);
                },
                error: function (err) {
                    if(options && options['error']) options['error'](err);
                }
            });
        }

    })
});