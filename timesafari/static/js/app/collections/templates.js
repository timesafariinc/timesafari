define([
    '../models/template'
], function (Model) {
    return Backbone.Collection.extend({

        url : 'template/',

        model: Model

    })
});