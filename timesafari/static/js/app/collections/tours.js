define([
    '../models/tour'
], function (Model) {
    return Backbone.Collection.extend({

        url : 'tour/',

        model: Model

    })
});