define([
    '../models/user'
], function (Model) {
    return Backbone.Collection.extend({

        url: 'user',

        model: Model,

        setParam: function (params) {
            var model = this;
            if(_.isString(params))
                params = [params];
            if(this.url.indexOf('?') < 0)
                this.url += '?';
            params.forEach(function (item) {
                model.url += item + '&';
            });
            this.url = this.url.replace(/\&$/, '');
        }

    })
});