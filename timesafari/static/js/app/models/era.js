define([
    'backbone-file-upload'
], function () {
    return Backbone.Model.extend({

        idAttribute: 'id',

        urlRoot: 'era/',

        fileAttribute: 'picture',

        url: function (isAdd) {
            var id = this.get('id'),
                url = this.urlRoot;
            if(isAdd) {
                url += id ? 'edit/' + id + '/' : 'add/';
            } else {
                url += id ? id + '/' : '';
            }
            return url;
        },

        defaults: {
            'id'            : undefined,
            'name'          : '',
            'picture'       : '',
            'price'         : '',
            'description'   : ''
        },

        save: function (attr, options) {
            if(this.validateEra()) {
                if(!options) options = {};
                if(this.get('id') == '') this.unset('id');
                options.url = this.url(true);
                options.formData = true;
                Backbone.Model.prototype.save.call(this, attr, options);
            }
        },

        validateEra: function() {
            var errors = {},
                success = [];
            if(!this.get('name'))
                errors['name'] = 'Это поле обязательное';
            if(this.get('name') && this.get('name').lenght > 30)
                errors['name'] = 'Длина этого поля не может превышать 30 символов';
            var price = +this.get('price');
            if(!price)
                errors['price'] = 'Введите пожалуйста числовое значение';
            if(price && (price < 0 || price > 999999))
                errors['price'] = 'Число должно быть в диапозоне от 0 до 999 999';
            if(!_.has(errors, 'name'))
                success.push('name');
            if(!_.has(errors, 'price'))
                success.push('price');
            App.Listener.trigger('EraModelIsValid', success);
            App.Listener.trigger('EraModelIsUnValid', errors);
            return _.isEmpty(errors)
        }
    })
});