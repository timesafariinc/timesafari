define([
    'backbone-file-upload'
], function () {
    return Backbone.Model.extend({

        idAttribute: 'id',

        urlRoot: 'kind/',

        fileAttribute: 'picture',

        defaults: {
            'id'            : null,
            'name'          : '',
            'price'         : '',
            'description'   : '',
            'picture'       : '',
            'era'           : null,
            'danger'        : 0,
            'count'         : 0
        },

        url: function (isAdd) {
            var id = this.get('id'),
                url = this.urlRoot;
            if(isAdd) {
                url += id ? 'edit/' + id + '/' : 'add/';
            } else {
                url += id ? id + '/' : '';
            }
            return url;
        },

        danger: {
            en: ['low', 'medium', 'high', 'very-high'],
            ru: ['НИЗКАЯ', 'СРЕДНЯЯ', 'ВЫСОКАЯ', 'ОЧЕНЬ ВЫСОКАЯ'],
            color: ['success', 'info', 'warning', 'danger']
        },

        getDanger: function(section) {
            if(!section) section = 'ru';
            var danger = parseInt(this.get('danger'));
            return this.danger[section][danger] ? this.danger[section][danger] : 'НЕ ИЗВЕСТЕН';
        },

        save: function (attr, options) {
            if(!options) options = {};
            if(this.get('id') == '') this.unset('id');
            options.url = this.url(true);
            options.formData = true;
            Backbone.Model.prototype.save.call(this, attr, options);
        },

        validate: function() {
            var errors = {},
                success = [];
            if(!this.get('name'))
                errors['name'] = 'Это поле обязательное';
            if(this.get('name') && this.get('name').lenght > 30)
                errors['name'] = 'Длина этого поля не может превышать 30 символов';
            var price = +this.get('price');
            if(!price)
                errors['price'] = 'Введите пожалуйста числовое значение';
            if(price && (price < 0 || price > 999999))
                errors['price'] = 'Число должно быть в диапозоне от 0 до 999 999';
            var count = +this.get('count');
            if(count == undefined)
                errors['count'] = 'Введите пожалуйста числовое значение';
            if(count && (count < 0 || count > 999999))
                errors['count'] = 'Число должно быть в диапозоне от 0 до 50';
            if(!this.get('era'))
                errors['era'] = 'Это поле обязательно';
            if(!_.has(errors, 'name'))
                success.push('name');
            if(!_.has(errors, 'price'))
                success.push('price');
            App.Listener.trigger('KindModelIsValid', success);
            App.Listener.trigger('KindModelIsUnValid', errors);
            if(!_.isEmpty(errors))
                return true;
        }

    })
});