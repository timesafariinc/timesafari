define([
    '../models/kind'
], function (Kind) {
    return Backbone.Model.extend({

        idAttribute: 'id',

        urlRoot : '/message/',

        url: function (isSave, isDelete) {
            var id = this.get('id');
            var result = this.urlRoot;
            if(isSave) {
                result = id ? 'edit/' + id + '/' : '/chat/send/';
            } else if(isDelete) {
                result += 'delete/' + id;
            } else {
                result += id;
            }
            return result;
        },

        save: function (attr, options) {
            if(!options) options = {};
            options.url = this.url(true);
            if(_.isObject(this.get('receiver')))
                this.set({assigned_personal: this.get('receiver')['id']});
            Backbone.Model.prototype.save.call(this, attr, options);
        },

        destroy: function (options) {
            if(!options) options = {};
            options.data = 'id=' + this.get('id');
            options.url = this.url(false, true);
            Backbone.Model.prototype.destroy.call(this, options);
        }

    })
});