define([
    '../models/kind'
], function (Kind) {
    return Backbone.Model.extend({

        idAttribute: 'id',

        urlRoot : '/tour/',

        defaults: {
            'name'          : '',
            'event_date'    : '',
            'client'        : null,
            'paid'          : false,
            'status'        : 0
        },

        validate: function() {
            var errors = {},
                success = [];
            if(!this.get('number'))
                errors['number'] = 'Это поле обязательное';
            if(this.get('number') && this.get('number').lenght > 20)
                errors['number'] = 'Длина этого поля не может превышать 20 символов';
            if(!this.get('last_name'))
                errors['last_name'] = 'Это поле обязательное';
            if(this.get('last_name') && this.get('last_name').lenght > 20)
                errors['last_name'] = 'Длина этого поля не может превышать 20 символов';
            if(!this.get('first_name'))
                errors['first_name'] = 'Это поле обязательное';
            if(this.get('first_name') && this.get('first_name').lenght > 20)
                errors['first_name'] = 'Длина этого поля не может превышать 20 символов';
            var month = +this.get('month'),
                year = +this.get('year'),
                code = +this.get('code');
            if(!month)
                errors['month'] = 'Введите пожалуйста числовое значение';
            if(month && (month < 1 || month > 12))
                errors['month'] = 'Число должно быть в диапозоне от 1 до 12';
            if(!year)
                errors['year'] = 'Введите пожалуйста числовое значение';
            if(year && (year < 16 || year > 20))
                errors['year'] = 'Число должно быть в диапозоне от 16 до 20';
            if(!code)
                errors['code'] = 'Введите пожалуйста числовое значение';
            if(!_.has(errors, 'number')) success.push('number');
            if(!_.has(errors, 'last_name')) success.push('last_name');
            if(!_.has(errors, 'first_name')) success.push('last_name');
            if(!_.has(errors, 'month')) success.push('month');
            if(!_.has(errors, 'year')) success.push('year');
            if(!_.has(errors, 'code')) success.push('code');
            App.Listener.trigger('PayModelIsValid', success);
            App.Listener.trigger('PayModelIsUnValid', errors);
            return _.isEmpty(errors);
        }

    })
});