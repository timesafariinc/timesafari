define([
    'jquery.cookie',
    'backbone-file-upload'
], function () {
    return Backbone.Model.extend({

        idAttribute: 'id',

        urlRoot: 'profile/',

        url: function () {
            return this.urlRoot;
        },

        groups: {
            1: 'admin',
            2: 'scientist',
            3: 'consultant',
            4: 'client'
        },

        hasAccess: function (rights) {
            if(rights instanceof String) {
                rights = [rights];
            }
            return rights.indexOf(this.get('right')) >= 0;
        },

        auth: function (success, error, fail) {
            var parent = this;
            if(this.isProfileValid(['email', 'password'], success, error)) {
                $.ajax({
                    url: '/login/',
                    type: 'POST',
                    data: this.toJSON()
                }).done(function (response) {
                    $.cookie('auth', response['auth_token']);
                    $.cookie('tour-create-era', '');
                    $.cookie('tour-create-kind', '');
                    $.cookie('tour-create-event-date', '');
                    $.ajaxSetup({
                        headers: {'Authorization': 'Token ' + $.cookie('auth')}
                    });
                    parent.set(response);
                    parent.trigger('login');
                }).fail(function (err) {
                    if(fail) fail(err['responseJSON']);
                    console.error(err);
                });
            }
        },

        registration: function (success, error, fail) {
            var parent = this;
            if(this.isProfileValid(['email', 'password'], success, error)) {
                $.ajax({
                    url: '/registration/',
                    type: 'POST',
                    data: this.toJSON()
                }).done(function (response) {
                    parent.set(response);
                    parent.auth();
                }).fail(function (err) {
                    if(fail) fail(err['responseJSON']);
                });
            }
        },
        
        logout: function () {
            this.clear();
            $.cookie('auth', '');
            $.ajaxSetup({ headers: {'Authorization': ''} });
            this.trigger('logout');
            App.Router.public.setRoute('home');
        },

        isAuth: function () {
            return this.get('id') ? true : false;
        },

        changeEmail: function (email, currentPassword, options) {
            if(!options) options = {};
            var parent = this;
            $.post('/profile/edit/email/', {new_email: email, current_password: currentPassword}).done(function () {
                parent.trigger('emailChanged', 'email', 'success', 'E-mail успешно изменен');
            }).fail(function (err) {
                if (options['fail'])
                    options['fail'](err['responseJSON']);
                console.error(err);
            });
        },

        changePassword: function (password, currentPassword, options) {
            var parent = this;
            $.post('/profile/edit/password/', {new_password: password, current_password: currentPassword}).done(function () {
                parent.trigger('passwordChanged', 'password', 'success', 'Пароль успешно изменен');
            }).fail(function (err) {
                if (options['fail'])
                    options['fail'](err['responseJSON']);
                console.error(err);
            });
        },
        
        change: function (formData, options) {
            $.ajax({
                url: '/profile/edit/',
                type: 'PUT',
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                data: formData
            }).done(function (res) {
                if (options['done'])
                    options['done'](res);
                App.Profile.set(res);
            }).fail(function (err) {
                if (options['fail'])
                    options['fail'](err['responseJSON']);
                console.error(err);
            })
        },
        
        getGroup: function () {
            var idGroup = this.get('groups');
            return idGroup && this.groups[idGroup] ? this.groups[idGroup] : 'client';
        },

        isPersonal: function () {
            var idGroup = this.get('groups')[0];
            return [1,2,3].indexOf(idGroup) >= 0;
        },

        validation: {
            password:  {
                required: true,
                msg: 'Поле является обязательным'
            },
            email:  {
                required: true,
                msg: 'Поле является обязательным'
            },
            last_name:  {
                required: true,
                msg: 'Поле является обязательным'
            },
            first_name:  {
                required: true,
                msg: 'Поле является обязательным'
            }
        },

        isProfileValid: function(fields, successCallback, errorCallback) {
            var parent = this;
            var errors = {};
            var success = [];
            fields.forEach(function(fieldName) {
                var field = parent['validation'][fieldName];
                if(field) {
                    var value = parent.get(fieldName);
                    if(field['required']) {
                        if(value) success.push(fieldName);
                        else errors[fieldName] = field['msg'];
                    }
                }
            });
            if(successCallback)
                successCallback(success);
            if(errorCallback)
                errorCallback(errors);
            return _.isEmpty(errors);
        }

    })
});