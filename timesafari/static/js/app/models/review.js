define([
    'backbone'
], function () {
    return Backbone.Model.extend({

        idAttribute: 'id',

        urlRoot : '/review/',

        url: function (isAdd) {
            var id = this.get('id'),
                url = this.urlRoot;
            if(isAdd) {
                url += id ? 'edit/' + id + '/' : 'add/';
            } else {
                url += id ? id + '/' : '';
            }
            return url;
        },

        save: function (attr, options) {
            if(!options) options = {};
            options.url = this.url(true);
            if(_.isObject(this.get('receiver')))
                this.set({assigned_personal: this.get('receiver')['id']});
            Backbone.Model.prototype.save.call(this, attr, options);
        }

    })
});