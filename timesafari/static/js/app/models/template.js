define([
    './tour',
    '../models/kind'
], function (Model, Kind) {
    return Model.extend({

        urlRoot : '/template/',

        defaults: {
            'name'          : '',
            'client'        : null,
            'status'        : 0
        },

        save: function (attr, options) {
            if(!options) options = {};
            options.url = this.url(true);
            Backbone.Model.prototype.save.call(this, attr, options);
        },

        destroy: function (options) {
            if(!options) options = {};
            options.data = 'id=' + this.get('id');
            options.url = this.url(false, true);
            Backbone.Model.prototype.destroy.call(this, options);
        },

        validate: function() {}

    })
});