define([
    '../models/kind'
], function (Kind) {
    return Backbone.Model.extend({

        idAttribute: 'id',

        url: function (isSave, isDelete) {
            var id = this.get('id');
            var result = this.urlRoot;
            if(isSave) {
                result += id ? 'edit/' + id + '/' : 'add/';
            } else if(isDelete) {
                result += 'delete/' + id;
            } else {
                result += id;
            }
            return result;
        },

        urlRoot : '/tour/',

        defaults: {
            'name'          : '',
            'event_date'    : '',
            'document_link' : '',
            'client'        : null,
            'paid'          : false,
            'status'        : 0
        },

        status: {
            0: 'waiting',
            1: 'processing',
            2: 'finish',
            3: 'passed'
        },

        danger: {
            en: ['low', 'medium', 'high', 'very-high'],
            ru: ['Низкая', 'Средняя', 'Высокая', 'Очень высокая'],
            color: ['success', 'info', 'warning', 'danger']
        },

        hasStatus: function (status) {
            var currentStatus = this.status[this.get('status')];
            if(_.isString(status))
                status = [status];
            var result = false;
            status.forEach(function (item) {
                if(item == currentStatus) result = true;
            });
            return result;
        },
        
        getStatusColor: function () {
            var result = '';
            switch (this.get('status')) {
                case 0: result = 'warning'; break;
                case 1: result = 'warning'; break;
                case 3: result = 'danger'; break;
            }
            if(this.get('paid'))
                result = 'success';
            return result;
        },
        
        getStatus: function () {
            var result = 'Неизвестен';
            switch (this.get('status')) {
                case 0: result = 'Ожидает подтверждения'; break;
                case 1: result = this.get('paid') ? 'Ожидает отправки' : 'Ожидает оплаты'; break;
                case 2: result = 'Завершен'; break;
                case 3: result = 'Отменен'; break;
            }
            return result;
        },

        getStatusHref: function () {
            var status = this.get('status');
            return status == 1 && !this.get('paid') ? 'tour/' + this.get('id') + '/pay' : '';
        },

        getStatusHrefConsultant: function () {
            var status = this.get('status');
            var result = 'tour/#STATUS#/' + this.get('id');
            // waiting
            if(status == 0)
                status = 'new';
            else if(status == 1 && !this.get('paid'))
                status = 'approved';
            else if(status == 1 && this.get('paid') || status == 2 || status == 3 )
                status = 'paid';
            return result.replace('#STATUS#', status);
        },

        getDanger: function(section) {
            if(!section) section = 'ru';
            var danger = parseInt(this.get('danger'));
            return this.danger[section][danger] ? this.danger[section][danger] : 'НЕ ИЗВЕСТЕН';
        },

        save: function (attr, options) {
            if(!options) options = {};
            if(_.isObject(this.get('assigned_personal')))
                this.set({assigned_personal: this.get('assigned_personal')['id']});
            if(_.isObject(this.get('client')))
                this.set({client: this.get('client')['id']});
            if(this.get('document_link'))
                this.unset('document_link');
            if(this.validate()) {
                $.ajax({
                    url: this.url(true),
                    type: this.get('id') ? 'PUT' : 'POST',
                    data: _.extend(this.toJSON(), attr),
                    success: _.bind(function (res) {
                        if(options && options['success'])
                            options['success'](res);
                    }, this),
                    error: _.bind(function (err) {
                        if(options && options['error'])
                            options['error'](err);
                    }, this)
                });
            }
        },

        destroy: function (options) {
            if(!options) options = {};
            options.data = 'id=' + this.get('id');
            options.url = this.url(false, true);
            Backbone.Model.prototype.destroy.call(this, options);
        },

        validate: function() {
            var errors = {},
                success = [];
            if(!this.get('event_date'))
                errors['event_date'] = 'Это поле обязательное';
            if(this.get('event_date') && this.get('event_date').lenght > 10)
                errors['event_date'] = 'Длина этого поля не может превышать 10 символов';
            if(!_.has(errors, 'event_date')) success.push('event_date');
            App.Listener.trigger('PayModelIsValid', success);
            App.Listener.trigger('PayModelIsUnValid', errors);
            return _.isEmpty(errors);
        },
        
        extendPayDate: function (options) {
            $.ajax({
                url: 'tour/extend/' + this.get('id') + '/',
                type: 'PUT',
                success: _.bind(function (res) {
                    if(options && options['success'])
                        options['success'](res);
                }, this),
                error: _.bind(function (err) {
                    if(options && options['error'])
                        options['error'](err);
                }, this)
            });
        }

    })
});