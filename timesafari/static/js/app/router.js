define([
    'backbone'
], function (Backbone) {
    return Backbone.Router.extend({

        prevRoute: null,
        currentRoute: null,

        routes: {
            ''                  : 'home',
            'home'              : 'home',
            'advantages'        : 'advantages',
            'market'            : 'market',
            'profile'           : 'profile',
            'tour/create'       : 'createTour',
            'tour/create/*prms' : 'createTour',
            'tour/:id/pay'      : 'payTour',
            'tour/:id/edit'     : 'editTour',
            'tour/:status/:id'  : 'viewTour',
            'era/create'        : 'createEra',
            'era/:id'           : 'viewEra',
            'era/:id/edit'      : 'createEra',
            'kind/create'       : 'createKind',
            'kind/:id'          : 'viewKind',
            'kind/:id/edit'     : 'createKind',
            'template/create'   : 'createTemplate',
            'template/:id'      : 'viewTemplate',
            'template/:id/edit' : 'createTemplate',
            'templates*params'  : 'templates',
            'tours*params'      : 'tours',
            'periods'           : 'periods',
            'kinds*params'      : 'kinds',
            'auth'              : 'auth',
            'logout'            : 'logout',
            'forgot'            : 'forgot'
        },

        initialize: function(menu) {
            this.menu = menu;
        },

        home: function () {
            App.Views['home'].render();
            $('body').animate({scrollTop:0}, 200);
        },

        advantages: function () {
            App.Views['home'].render();
            $('body').animate({scrollTop:$('.promo-second').position().top}, 400);
        },

        market: function () {
            App.Views['home'].render();
            $('body').animate({scrollTop:$('.promo-third').position().top + 50}, 500);
        },

        createTour: function (id, params) {
            App.Views['Tour.Create'].render(params);
        },

        payTour: function (id) {
            App.Views['Tour.Pay'].render(id);
        },

        viewTour: function (status, id) {
            if(this.prevRoute == 'tours')
                App.Views['Tour.View'].isTable = true;
            App.Views['Tour.View'].render('tour', status, id);
        },

        createKind: function (id) {
            App.Views['Kind.Create'].render(id);
        },

        viewKind: function (id) {
            if(this.prevRoute == 'kinds')
                App.Views['Kind.View'].isTable = true;
            App.Views['Kind.View'].render(id);
        },

        createEra: function (id) {
            App.Views['Era.Create'].render(id);
        },

        viewEra: function (id) {
            if(this.prevRoute == 'periods')
                App.Views['Era.View'].isTable = true;
            App.Views['Era.View'].render(id);
        },

        createTemplate: function (id) {
            App.Views['Template.Create'].render(id);
        },

        viewTemplate: function (id) {
            setTimeout(function() {
                App.Views['Template.View'].render(id);
            }, 500);
        },

        profile: function () {
            if(App.Views['Profile']) {
                App.Views['Profile'].render();
                //$('body').animate({scrollTop:0}, 200);
            } else
                this.setRoute('home');
        },

        auth: function () {
            if(App.Profile.isAuth())
                this.setRoute('profile');
            else {
                App.Views['login'].render();
                App.Views['login'].delegateEvents();
            }
        },

        logout: function () {
            App.Profile.logout();
        },

        templates: function (filter) {
            App.Views['Templates'].render(filter);
        },

        tours: function (filter) {
            App.Views['Tours'].render(filter);
        },

        periods: function () {
            App.Views['Era'].render();
        },

        kinds: function (filter) {
            App.Views['Kinds'].render(filter);
        },

        execute: function(callback, args, name, a) {
            if(App.Profile && App.Profile.isAuth() || 'auth|home|advantages|market'.indexOf(name) >= 0) {
                if(App.Views['Profile']) {
                    if('home|advantages|market'.indexOf(name) >= 0)
                        App.Views['Profile'].removeHeader();
                    else
                        App.Views['Profile'].renderHeader();
                }
                this.prevRoute = this.currentRoute ? this.currentRoute : name;
                this.currentRoute = Backbone.history.fragment;
                App.Views['Menu'].setActive(name);
                if(callback) callback.apply(this, args);
            } else {
                this.setRoute('home');
            }
        },
        
        setRoute: function (route, trigger) {
            trigger = _.isNull(trigger) || _.isEmpty(trigger) ? true : trigger;
            this.navigate('#' + route, {trigger: trigger});
        },

        prevRouter: function () {
            if(this.prevRoute)
                this.navigate('#' + this.prevRoute, {trigger: true});
            else if(App.Views['Profile'])
                this.setRoute('profile');
            else
                this.setRoute('home');
        }
    })
});