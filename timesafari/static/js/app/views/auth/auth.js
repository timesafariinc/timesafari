define([
    '../modal'
], function (Modal) {
    return Modal.extend({

        template: _.template($('#auth\\.tpl').html()),

        currentPage: 'auth',

        events: {
            'click #registration'   : 'registration',
            'click #login'          : 'login',
            'click #allowed'        : 'allowed'
        },

        initialize: function () {
            this.model = App.Profile;
            Backbone.Validation.bind(this);
        },

        registration: function (e) {
            var parent = this;
            var fields = [];
            $(e.target).closest('.col-md-6').find('input').each(function (i, item) {
                var field = [$(item).attr('name'), $(item).val()];
                fields.push(field);
            });
            App.Profile.set(_.object(fields)).registration(
                function (fields) {
                    parent.success($(e.target).closest('form'), fields);
                    // success
                },
                function (fields) {
                    parent.error($(e.target).closest('form'), fields);
                    // error
                },
                function (err) {
                    parent.fail($(e.target).closest('form'), err);
                }
            );
        },
        
        login: function (e) {
            var parent = this;
            var fields = [];
            $(e.target).closest('.col-md-6').find('input').each(function (i, item) {
                var field = [$(item).attr('name'), $(item).val()];
                fields.push(field);
            });
            App.Profile.set(_.object(fields)).auth(
                function (fields) { // success
                    parent.success($(e.target).closest('form'), fields);
                },
                function (fields) {
                    // error
                    parent.error($(e.target).closest('form'), fields);
                },
                function (err) {
                    parent.fail($(e.target).closest('form'), err);
                }
            );
        },

        success: function (form, fields) {
            var parent = this;
            fields.forEach(function (field) {
                var $input = $(form).find('[name="' + field + '"]');
                $input.parent().removeClass('has-error has-success').addClass('has-success');
                $input.next().html('');
            });
        },

        error: function (form, fields) {
            _.each(fields, function (field, key) {
                var $input = $(form).find('[name="' + key + '"]');
                $input.parent().removeClass('has-error has-success').addClass('has-error');
                $input.next().html(field);
            });
        },

        errors: {
            'time safari user with this email address already exists.' : 'Пользователь с таким E-mail уже существует',
            'Enter a valid email address.' : 'Введите корректные E-mail адрес',
            'Unable to login with provided credentials.' : 'Не существует такой комбинации E-mail и пароля'
        },

        fail: function (form, fields) {
            var $block = $('#auth-error');
            _.each(fields, _.bind(function (field, key) {
                field = this.errors[field] ? this.errors[field] : field;
                $block.find('.text-error').text(field);
            }, this));
            $block.removeClass('hidden');
        },

        cancel: function () {
            if(this.checkCurrentPage(this.currentPage)) {
                this.$el.modal('hide');
                App.Router.public.setRoute('profile');
            }
        },

        allowed: function (e) {
            var checked = $(e.target).prop('checked');
            if(checked)
                this.$el.find('#registration').removeAttr('disabled');
            else
                this.$el.find('#registration').attr('disabled', 'disabled');
        }
    })
});