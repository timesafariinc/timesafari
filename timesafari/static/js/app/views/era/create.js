define([
    '../modal',
    '../../models/era'
], function (Modal, Model) {
    return Modal.extend({

        template: _.template($('#era\\.create\\.modal\\.tpl').html()),

        events: {
            'click #era-create #cancel'         : 'cancel',
            'click #era-create .cancel-icon'    : 'cancel',
            'click .era-create'                 : 'create'
        },

        pressCreate: false,
        isRender: false,

        initialize: function () {
            Modal.prototype.initialize.call(this, 'Era');
        },

        render: function (id) {
            var parent = this;
            if(!this.isRender) {
                this.model = new Model();
                this.isRender = true;
            }
            if(id && _.isString(id)) {
                this.model.set({id:id}).fetch({success: function () {
                    parent.render();
                }});
                return this;
            }
            parent.isRender = false;
            Backbone.Validation.bind(this);
            Modal.prototype.render.call(this);

        },

        create: function (e) {
            var parent = this,
                fields = {};
            this.$el.find('textarea,input').each(function (i, item) {
                if($(item).is(':file') && $(item).val())
                    fields[$(item).attr('name')] = item.files[0];
                else
                    fields[$(item).attr('name')] = $(item).val();
            });
            this.model.set(fields).save(null, {success: function (model) {
                parent.trigger('EraCreated', model);
                parent.cancel();
            }});
        },

        cancel: function (e) {
            this.$el.modal('hide');
            if(this.isTable) {
                App.Router.public.prevRouter();
            } else
                App.Router.public.setRoute('profile');
        }

    })
});
