define([
    'backbone'
], function (ViewTour) {
    return Backbone.View.extend({

        tagName: 'div',

        className: 'panel profile-block',

        name: 'Название',

        events: {
            'click .view-tour'      : 'viewTour'
        },

        template: _.template(
            '<div class="panel-heading"><div class="panel-title"><%- name %></div></div>' +
            '<div class="panel-body"></div>' +
            '<%if(addAction) {%><div class="panel-footer text-right"><a class="btn" href="#<%-addAction%>">Создать</a></div><%}%>'
        ),

        template_item: _.template(
            '<div class="row">' +
                '<% if(picture) { %><div class="col-md-3"><img src="<%- picture %>"></div><% } %>' +
                '<div class="col-md-<% picture ? "8" : "11"%>"></div>' +
                '<div class="col-md-1"><span class="more-icon"></span></div>' +
            '</div>'
        ),

        template_row: {
            icon    : _.template('<div class="row form-group"><div class="col-md-12"><%-value%></div></div>'),
            general : _.template(
                '<div class="row form-group">' +
                    '<div class="col-md-<%- action ? "10" : "12" %> items"></div>' +
                    '<% if(action) { %>' +
                            '<div class="col-md-2 view-tour" data-url="<%-action%>">' +
                            '<% if(actionIcon) { %>' +
                                '<%= actionIcon %>' +
                            '<% } else { %>' +
                                '<span data-id="<%-id%>" class="glyphicon glyphicon-menu-right"></span>' +
                            '<% } %>' +
                        '</div>' +
                    '<% } %>' +
                '</div>'),
            name    : _.template('<div class="row"><div class="col-md-12"><strong><%=value%></strong></div></div>'),
            status  : _.template(
                '<div class="row">' +
                    '<div class="col-md-12">' +
                        'СТАТУС: <span class="text-<%-color%>"><%-value%></span>' +
                    '</div>' +
                '</div>'),
            event_date: _.template('<div class="row"><div class="col-md-12">ДАТА ПРЫЖКА: <%-value%></div></div>'),
            score   : _.template('<div class="row"><div class="col-md-12">ОЦЕНКА: <span class="star-<%-value%>"></span></div></div>'),
            row     : _.template('<div class="row"><div class="col-md-12"><%=value%></div></div>')
        },

        initialize: function (defaults) {
            this.id         = defaults['id'];
            this.name       = defaults['name'];
            this.icon       = defaults['icon'];
            this.fields     = defaults['fields'];
            this.action     = defaults['action'];
            this.actionIcon = defaults['actionIcon'];
            this.actionClass= defaults['actionClass'];
            this.addAction  = defaults['addAction'];
            this.collection = defaults['collection'];
            this.filter     = defaults['filter'];
            this.listenTo(this.collection, 'reset', this.resetCollection);
            this.listenTo(App.Views['Tour.Create'], 'TourCreated', this.addTour);
        },

        addTour: function (model) {
            this.collection.add(model);
            this.resetCollection(this.collection);
        },

        resetCollection: function (collection) {
            var $body = this.$el.find('.panel-body').html('');
            var parent = this;
            if(this.filter)
                collection = collection.filter(this.filter);
            if(!collection.length) {
                $body.append($('<div/>').addClass('text-center').text('Записей не найденно'))
            }
            collection.slice(0, 5).forEach(function (model) {
                $body.append(parent['template_row']['general']({
                    action      : parent.action,
                    actionIcon  : parent.actionIcon,
                    actionClass : parent.actionClass,
                    id: model.get('id')
                }));
                parent.fields.forEach(function (field) {
                    var template = parent['template_row'][field];
                    var value = model.get(field);
                    if(!_.isNull(value)) {
                        if(value && _.isString(value)) value = value.replace(/\n/g, '<br>');
                        value = {value: value, id: model.get('id')};
                        if(!template) template = parent['template_row']['row'];
                        switch (field) {
                            case 'status':
                                value['color'] = model.getStatusColor();
                                value['value'] = model.getStatus();
                                value['href'] = model.getStatusHref();
                            break;
                            case 'sender':
                            case 'client':
                                value['value'] = model.get(field)['last_name'] + ' ' + model.get(field)['first_name'];
                            break;
                            case 'danger':
                                value['value'] = 'ОПАСТНОСТЬ: ' + model.getDanger();
                                value['href'] = model.getDanger();
                            break;
                            case 'era':
                                value['value'] = 'ПЕРИОД: ' + value['value']['name'];
                            break;
                            case 'updated':
                                var date = value['value'].split('T');
                                var time = date[1].split(':');
                                value['value'] = 'ВРЕМЯ: ' + date[0] + ' ' + time[0] + ':' + time[1];
                            break;
                        }
                        $body.find('.items:last').append(template(value));
                    }
                })
            });
        },

        render: function () {
            this.collection.fetch({reset: true});
            this.$el.html(this.template({name: this.name, addAction: this.addAction}));
            return this;
        },

        viewTour: function (e) {
            var id = $(e.currentTarget).children().data('id');
            App.Router.public.setRoute(this.id.replace(/\./g, '/') + '/' + id);
        }

    })
});