define([
    './panel',
    './setting/panel',
    './profile.head'
], function (Panel, Settings, Head) {
    return Backbone.View.extend({

        el: '#general-page',

        template: _.template(
            '<div class="row profile-heading"></div>' +
            '<div class="container"><div class="row panels">' +
                '<div class="col-md-4 left-panel"></div>' +
                '<div class="col-md-4 center-panel"></div>' +
                '<div class="col-md-4 right-panel"></div>' +
            '</div></div>'
        ),

        initialize: function (panels) {
            this.listenTo(App.Listener, 'editSettings', _.bind(this.editSettings, this));
            this.head = new Head();
            panels = panels.panels;
            this.settings = new Settings();
            if(panels[0]) {
                this.left = new Panel(panels[0]);
            }
            if(panels[1]) {
                this.center = new Panel(panels[1]);
            }
            if(panels[2]) {
                this.right = new Panel(panels[2]);
            }
        },

        render: function () {
            var parent = this;
            this.$el.html(this.template());
            if(this.left) {
                this.$el.find('.left-panel').html(this.left.render().el);
                this.left.delegateEvents();
            }
            if(this.center) {
                this.$el.find('.center-panel').html(this.center.render().el);
                this.center.delegateEvents();
            }
            if(this.right) {
                this.$el.find('.right-panel').append(this.right.render().el);
                this.right.delegateEvents();
            }
            this.head.render();
        },

        removeHeader: function () {
            this.head.removeContent();
        },

        renderHeader: function () {
            this.head.render();
        },

        editSettings: function (e) {
            console.log()
            this.settings.show($(e.currentTarget).data('el'));
        }

    })
});