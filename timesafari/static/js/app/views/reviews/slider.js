define([
    'bootstrap',
    '../../collections/reviews'
], function (bootstrap, Collection) {
    return Backbone.View.extend({

        className: 'carousel',

        id: 'carousel-reviews',

        attributes: {
            'data-ride': 'carousel'
        },

        template: _.template($('#review\\.slider\\.tpl').html()),

        template_item: _.template($('#review\\.slider\\.item\\.tpl').html()),

        initialize: function () {
            this.collection = new Collection();
        },

        render: function (id) {
            this.$el.html(this.template());
            this.collection.fetch({success: _.bind(this.renderData, this)});
        },
        
        renderData: function (collection) {
            collection.forEach(_.bind(function (model, index) {
                model = model.toJSON();
                if(_.isObject(model.client) && !model.client.picture)
                    model.client.picture = "/static/img/no_ava.gif";
                this.$el.find('.carousel-inner').append(
                    this.template_item(_.extend(model, {active: !index}))
                );
            }, this));
            this.$el.carousel({
                interval: 10000
            });
            this.trigger('renderData');
        }
    })
});
