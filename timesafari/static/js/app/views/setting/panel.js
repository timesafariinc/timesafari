define([
    './profile',
    './personalInfo'
], function (Profile, PersonalInfo) {
    return Backbone.View.extend({

        tagName: 'div',

        className: 'panel profile-block',

        name: 'Название',

        events: {
        },

        template: _.template(
            '<div class="panel-heading"><div class="panel-title">Настройки</div></div>' +
            '<div class="panel-body" style="font-size: 10px">' +
                '<div class="editSettings" data-el="profile">РЕДАКТИРОВАТЬ ДАННЫЕ УЧЕТНОЙ ЗАПИСИ</div>' +
                '<div class="editSettings" data-el="personalInfo">РЕДАКТИРОВАТЬ ПЕРСОНАЛЬНУЮ ИНФОРМАЦИЮ</div>' +
                '<div class="editSettings"><a href="#logout">ВЫЙТИ ИЗ ПРОФИЛЯ</a></div>' +
            '</div>'
        ),

        render: function () {
            this.$el.html(this.template());
            return this;
        },

        initialize: function () {
            this.profile = new Profile();
            this.personalInfo = new PersonalInfo();
        },
        
        show: function (el) {
            if(this[el])
                this[el].show();
        }

    })
});