define([
    '../modal'
], function (Modal) {
    return Modal.extend({

        template: _.template($('#settings\\.profile\\.tpl').html()),

        events: {
            'click .changeEmail'                    : 'changeEmail',
            'click .changePassword'                 : 'changePassword',
            'click #setting-profile .cancel-icon'   : 'cancel'
        },

        initialize: function () {
            this.listenTo(App.Profile, 'emailChanged', this.profileChanged);
            this.listenTo(App.Profile, 'emailChangedError', this.profileChanged);
            this.listenTo(App.Profile, 'passwordChanged', this.profileChanged);
            this.listenTo(App.Profile, 'passwordChangedError', this.profileChanged);
        },

        render: function () {
            if(App.Profile) {
                this.$el.find('.modal-body').html(this.template(App.Profile.toJSON()));
            }
        },

        show: function () {
            this.render();
            this.$el.modal('show');
        },

        cancel: function (e) {
            this.$el.modal('hide');
        },

        changeEmail: function (e) {
            var $value = $(e.currentTarget).closest('.row').prev().find('[name="email"]');
            var $currentPassword = $(e.currentTarget).closest('.row').prev().find('[name="currentPassword"]');
            if(!this.validate($value, $currentPassword)) {
                return this;
            }
            App.Profile.changeEmail($value.val(), $currentPassword.val(), {
                fail: function (err) {
                    var $input = $(e.target).closest('.row').prev().prev().children();
                    $input.html('');
                    _.each(err, function (item) {
                        $input.append($('<div/>').text(item[0]));
                    });
                }
            });
        },

        changePassword: function (e) {
            var $value = $(e.target).closest('.row').prev().find('[name="password"]');
            var $currentPassword = $(e.target).closest('.row').prev().find('[name="currentPassword"]');
            if(!this.validate($value, $currentPassword)) {
                return this;
            }
            App.Profile.changePassword($value.val(), $currentPassword.val(), {
                fail: function (err) {
                    var $input = $(e.target).closest('.row').prev().prev().children();
                    $input.html('');
                    _.each(err, function (item) {
                        $input.append($('<div/>').text(item[0]));
                    });
                }
            });
        },

        profileChanged: function (el, status , message) {
            if(!message)
                message = status == 'success' ? 'Успешно измененн' : 'Ошибка изменения';
            this.$el.find('button:' + (el == 'email' ? 'first' : 'last')).parent().find('small').remove();
            this.$el.find('button:' + (el == 'email' ? 'first' : 'last')).after(
                    $('<small/>').addClass(status == 'success' ? 'text-success' : 'text-danger').text(' ' + message)
            );
            this.removeSpan();
        },

        removeSpan: function () {
            var parent = this;
            setTimeout(function () {
                parent.$el.find('.row:has(button) small').animate({opacity: 0}, function (e) {
                    $(this).remove();
                });
            }, 2000);
        },

        validate: function (first, second) {
            var $first_name = $(first),
                $last_name = $(second);
            var result = true;
            $first_name.parent().removeClass('has-error has-success');
            $last_name.parent().removeClass('has-error has-success');
            $first_name.next().remove();
            $last_name.next().remove();
            if(!$first_name.val()) {
                $first_name.parent().addClass('has-error');
                $first_name.after($('<small/>').addClass('text-danger').text('Это поле обязательное'));
                result = false;
            } else {
                $first_name.parent().addClass('has-success');
            }
            if(!$last_name.val()) {
                $last_name.parent().addClass('has-error');
                $last_name.after($('<small/>').addClass('text-danger').text('Это поле обязательное'));
                result = false;
            } else {
                $last_name.parent().addClass('has-success');
            }
            return result;
        }
    })
});
