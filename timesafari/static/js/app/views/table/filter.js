define([
    'backbone',
    'bootstrap'
], function (Backbone) {
    return Backbone.View.extend({

        tagName : 'div',

        className: 'filter',

        template: {
            //client      : _.template($('#filter\\.tour\\.client\\.tpl').html()),
            //consultant  : _.template($('#filter\\.tour\\.consultant\\.tpl').html()),
            //scientist   : _.template($('#filter\\.tour\\.consultant\\.tpl').html()),
            //admin       : _.template($('#filter\\.tour\\.consultant\\.tpl').html())
        },

        render: function (filter) {
            var group = App.Profile.getGroup();
            this.$el.html(this.template[group]());
            this.setDefaultValues(filter);
            return this;
        },
        
        setDefaultValues: function (filter) {
            
        }

    })
});