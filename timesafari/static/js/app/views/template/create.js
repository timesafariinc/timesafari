define([
    '../modal',
    '../../models/template',
    '../../collections/kinds'
], function (Modal, Model, Kinds) {
    return Modal.extend({

        template: _.template($('#template\\.create\\.tpl').html()),

        currentPage: 'template/create',

        events: {
            'click #template-create .cancel-icon'   : 'cancel',
            'click #templateSave'                   : 'create',
            'change #templateCreate [name="era"]'   : 'changeEra',
            'change #templateCreate [name="kind"]'  : 'changeKind'
        },

        pressCreate: false,

        initialize: function () {
            this.model = new Model();

            this.era = new Backbone.Collection();
            this.era.url = '/era/';
            this.listenTo(this.era, 'reset', this.setEra);

            this.kinds = new Kinds();
            this.listenTo(this.kinds, 'reset', this.setKinds);
        },
        
        initializeFromCookie: function (model) {
            if($.cookie('template-create-era'))
                model.set({era: $.cookie('tour-create-era')});
            if($.cookie('template-create-kind'))
                model.set({kind: $.cookie('tour-create-kind')});
        },
        
        render: function (id) {
            this.initializeFromCookie(this.model);
            this.afterRender = true;
            if(id && _.isString(id)) {
                this.model.set({id: id}).fetch({success: _.bind(this.render, this)});
                return;
            }
            Modal.prototype.render.call(this);
            this.era.fetch({reset:true});
            this.kinds.fetch({reset:true});
        },

        setEra: function (collection) {
            $select = this.$el.find('[name="era"]');
            collection.forEach(function (model) {
                $select.append($('<option>').attr('value', model.get('id')).text(model.get('name')));
            });
            if(this.model.get('era'))
                $select.val(this.model.get('era'));
            else
                $select.trigger('change');
        },

        setKinds: function () {
            this.$el.find('[name="era"]').trigger('change');
        },

        changeEra: function (e) {
            var id = parseInt($(e.currentTarget).val());
            $select = this.$el.find('[name="kind"]');
            $select.html('');
            this.kinds.filter(function (model) {
                return model.get('era')['id'] == id && model.get('count') > 0;
            }).forEach(function (model) {
                $select.append($('<option>')
                    .attr('value', model.get('id'))
                    .text(model.get('name'))
                );
            });
            if(this.model.get('era') && this.afterRender) {
                this.afterRender = false;
                $select.val(this.model.get('kind'));
            }
            $select.trigger('change', $select);
        },

        changeKind: function (e, select) {
            var id = select ? $(select).val() : $(e.target).val();
            var img = '',
                danger = '';
            if(id) img = this.kinds.get(id).get('picture');
            if(id) danger = {
                id: this.kinds.get(id).get('danger'),
                name: this.kinds.get(id).getDanger(),
                color: this.kinds.get(id).getDanger('color'),
                price: this.kinds.get(id).get('price')
            };
            if(!img && this.$el.find('[name="era"]').val())
                img = this.era.get(this.$el.find('[name="era"]').val()).get('picture');
            this.$el.find('.tour-create-image img').remove();
            if(img) {
                this.$el.find('.tour-create-image').append($('<img/>').attr({src: img}));
            }
            if(danger) {
                this.$el.find('[name="danger"]').val(danger['id'])
                    .prev().text(danger['name'])
                    .parent().removeClass('text-danger text-info text-warning text-success')
                    .addClass('text-' + danger['color']);
                var eraPrice = this.getPriceEra();
                this.$el.find('[name="price"]').val(parseInt(danger['price']) + (eraPrice ? eraPrice : 0));
                this.$el.find('#price').text(danger['price']);
            }
        },

        getPriceEra: function () {
            var result = 0;
            var era = this.$el.find('[name="era"]').val();
            var model = this.era.get(era);
            if(model && model.get('price')) {
                result = parseInt(model.get('price'));
            }
            return result;
        },

        create: function (e) {
            var fields = {},
                parent = this;
            this.$el.find('form input,select').each(function (i, item) {
                fields[$(item).attr('name')] = $(item).val();
            });
            fields['name'] = this.era.get(fields['era']).get('name') + '|'  + this.kinds.get(fields['kind']).get('name');
            if(!this.model)
                this.model = new Model();
            this.model.set(fields).save(null, {
                success: function (model) {
                    parent.trigger('templateCreated', model);
                    parent.timeout = 500;
                    parent.cancel();
                },
                error: function (model, err) {
                    if(err && err['responseJSON'])
                        parent.$el.find('#template-error').text(err['responseJSON']['detail'])
                }
            });
        },

        cancel: function (e) {
            if(this.checkCurrentPage(this.currentPage)) {
                this.$el.modal('hide');
                if(!this.timeout) {
                    App.Router.public.prevRouter();
                } else {
                    setTimeout(_.bind(function() {
                        App.Router.public.prevRouter();
                    }, this), this.timeout);
                }
            }
        }
    })
});
