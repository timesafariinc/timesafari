define([
    '../modal',
    '../../models/template',
    '../../models/era',
    '../../models/kind'
], function (Modal, Model, Era, Kind) {
    return Modal.extend({

        template: _.template($('#template\\.view\\.tpl').html()),

        currentPage: 'template',

        events: {
            'click #templateView .cancel-icon'  : 'cancel',
            'click #deleteTemplate'             : 'deleteTemplate'
        },

        blockCancel: false,

        pressChange: false,

        render: function (id) {
            var parent = this;
            var model = this.model = new Model();
            if(parent.emptyTour) {
                this.model.set({id: '', client: {first_name: '', last_name: 'Не определен', patronimic: ''}, kind: ''});
                this.renderData(this.model);
                parent.emptyTour = false;
            }
            else
                model.set({id: id}).fetch({success: _.bind(this.renderData, this)});
            return this;
        },
        
        renderData: function (model) {
            if(!model.has('kind')) model.set({kind: ''});
            if(!model.has('price')) model.set({price: ''});
            var merge = {};
            merge['status'] = {name: model.getStatus(), color: model.getStatusColor()};
            merge['danger'] = {name: model.getDanger(), color: model.getDanger('color')};
            if(this['status'] == 'passed') {
                merge['contain'] = this.message.get('content').replace(/[\\\n]/g, '<br>');
            }
            this.$el.find('.modal-body').html(this.template(_.extend(model.toJSON(), merge)));
            this.$el.modal();
            this.era = new Era();
            this.kind = new Kind();
            this.kind.set({id: model.get('kind')}).fetch({success: _.bind(this.setKind, this)});
        },

        setKind: function (model) {
            var era = model.get('era');
            if(era && _.isObject(era))
                this.$el.find('.era').html(era['name']);
            this.$el.find('.kind').text(model.get('name'));
        },

        deleteTemplate: function (e) {
            var parent = this;
            this.model.destroy({
                success: function () {
                    parent.cancel();
                }
            });
        },

        showError: function (err) {
            if(_.isEmpty(err))  return;
            if(_.isObject(err)) err = _.values(err)[0];
            if(_.isArray(err))  err = err[0];
            var errorMsg = '<div class="alert alert-danger error-message text-center" role="alert">' + err + '</div>';
            this.$el.find('.error-message').remove();
            this.$el.find('.row:first').before(errorMsg);
        },

        cancel: function (e) {
            if(this.checkCurrentPage(this.currentPage)) {
                this.$el.modal('hide');
                App.Router.public.setRoute('templates');
            }
        }

    })
});
