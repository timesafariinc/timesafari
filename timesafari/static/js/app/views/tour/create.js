define([
    '../modal',
    '../../models/tour',
    '../../models/template',
    '../../collections/kinds',
    'datepicker'
], function (Modal, Model, Template, Kinds) {
    return Modal.extend({

        template: _.template($('#tour\\.create\\.modal\\.tpl').html()),

        currentPage: 'tour/create',

        events: {
            'click #tour-create .cancel-icon'       : 'cancel',
            'click #tour-create+div #tourCancel'    : 'cancel',
            'click #tourSave'                       : 'create',
            'change #tour-create [name="era"]'      : 'changeEra',
            'change #tour-create [name="kind"]'     : 'changeKind',
            'change .date input'                    : 'changeDate'
        },

        pressCreate: false,

        params: {},

        initialize: function () {
            this.model = new Model();

            this.era = new Backbone.Collection();
            this.era.url = '/era/';
            this.listenTo(this.era, 'reset', this.setEra);

            this.kinds = new Kinds();
            this.listenTo(this.kinds, 'reset', this.setKinds);
        },
        
        initializeFromCookie: function (model) {
            if($.cookie('tour-create-era'))
                model.set({era: $.cookie('tour-create-era')});
            if($.cookie('tour-create-kind'))
                model.set({kind: $.cookie('tour-create-kind')});
            var event_date = $.cookie('tour-create-event-date');
            model.set({'event_date': event_date ? event_date : ''});
        },
        
        render: function (params) {
            this.mtemplate = null;
            this.initializeFromCookie(this.model);
            this.afterRender = true;
            this.setParams(params);
            Modal.prototype.render.call(this);
            this.$el.find('#event-date').datepicker({
                format: 'yyyy-mm-dd'
            });
            if(this.model.get('repeat')) {
                this.$el.find('input:not([name="event_date"]), select').attr('disabled', 'disabled');
            }
            this.era.fetch({reset:true});
            this.kinds.fetch({reset:true});
        },

        setParams: function (params) {
            var parent = this,
                isChange = false;
            if(params && _.isString(params)) {
                params = params.replace(/\?/g, '').split('&');
                params.forEach(function (item) {
                    if(item.indexOf('=') > 0) {
                        parent.params[item.split('=')[0]] = item.split('=')[1];
                        parent.model.set(item.split('=')[0], item.split('=')[1]);
                        isChange = true;
                    }
                });
            }
            var template = this.model.get('template');
            this.model.set({template: ''});
            if(template) {
                this.model.set({repeat: true});
                this.mtemplate = new Template();
                this.mtemplate.set({id: template}).fetch({
                    async: false,
                    success: _.bind(function (model) {
                        this.model.set(_.pick(model.toJSON(), 'era', 'kind', 'danger', 'price'));
                        this.model.set({template: template});
                    }, this)
                })
            }
            if(isChange) this.model.set('event_date', '');
        },

        setEra: function (collection) {
            $select = this.$el.find('[name="era"]');
            collection.forEach(function (model) {
                $select.append($('<option>').attr('value', model.get('id')).text(model.get('name')));
            });
            if(this.model.get('era'))
                $select.val(this.model.get('era'));
            else
                $select.trigger('change');
        },

        setKinds: function (collection) {
            this.$el.find('[name="era"]').trigger('change');
        },

        changeEra: function (e) {
            var parent = this;
            var id = parseInt($(e.currentTarget).val());
            $select = this.$el.find('[name="kind"]');
            $select.html('');
            var description = this.era.get(id).get('description');
            if(description) {
                this.$el.find('.dashboard:first p').text(description);
            }
            this.kinds.filter(function (model) {
                return model.get('era')['id'] == id && model.get('count') > 0
                    || parent.model.get('kind') == model.get('id');
            }).forEach(function (model) {
                $select.append($('<option>')
                    .attr('value', model.get('id'))
                    .text(model.get('name'))
                );
            });
            $.cookie('tour-create-era', id ? id : '');
            if(this.model.get('era') && this.afterRender) {
                this.afterRender = false;
                $select.val(this.model.get('kind'));
            }
            $select.trigger('change', $select);
        },

        changeKind: function (e, select) {
            var id = select ? $(select).val() : $(e.target).val();
            var img = '',
                danger = '';
            if(id) {
                img = this.kinds.get(id).get('picture');
                var description = this.kinds.get(id).get('description');
                this.$el.find('.dashboard:last p').text(description ? description : '');
            }
            if(id) danger = {
                id: this.kinds.get(id).get('danger'),
                name: this.kinds.get(id).getDanger(),
                color: this.kinds.get(id).getDanger('color'),
                price: this.kinds.get(id).get('price')
            };
            if(!img && this.$el.find('[name="era"]').val())
                img = this.era.get(this.$el.find('[name="era"]').val()).get('picture');
            if(img) {
                this.$el.find('.tour-create-image img').attr({src: img});
            }
            if(danger) {
                this.$el.find('[name="danger"]').val(danger['id'])
                    .prev().text(danger['name'])
                    .parent().removeClass('text-danger text-info text-warning text-success')
                    .addClass('text-' + danger['color']);
                var eraPrice = this.getPriceEra();
                this.$el.find('[name="price"]').val(parseInt(danger['price']) + (eraPrice ? eraPrice : 0));
                this.$el.find('#price').text(danger['price']);
            }
            $.cookie('tour-create-kind', id ? id : '');
        },

        changeDate: function (e) {
            $.cookie('tour-create-event-date', $(e.target).val());
        },
        
        getPriceEra: function () {
            var result = 0;
            var era = this.$el.find('[name="era"]').val();
            var model = this.era.get(era);
            if(model && model.get('price')) {
                result = parseInt(model.get('price'));
            }
            return result;
        },

        create: function (e) {
            var fields = {},
                parent = this;
            this.$el.find('form input,select').each(function (i, item) {
                fields[$(item).attr('name')] = $(item).val();
            });
            fields['name'] = this.era.get(fields['era']).get('name') + '|'  + this.kinds.get(fields['kind']).get('name');
            var model = this.model = new Model();
            model.set(fields);
            model.save(this.params, {
                success: function (model) {
                    parent.trigger('tourCreated', model);
                    parent.cancel(null, true);
                },
                error: function (err) {
                    if(err && err['responseJSON']) {
                        parent.$el.find('.error-row').removeClass('hidden');
                        parent.$el.find('.text-error').text(err['responseJSON']['detail']);
                    }
                }
            });
        },

        cancel: function (e, isCreate) {
            if(this.checkCurrentPage(this.currentPage)) {
                this.$el.modal('hide');
                setTimeout(_.bind(function () {
                    if(this.mtemplate && !isCreate)
                        App.Router.public.prevRouter();
                    else
                        App.Router.public.setRoute('profile');
                }, this), 300);
            }
        }

    })
});
