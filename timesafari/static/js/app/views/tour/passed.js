define([
    '../modal',
    '../../models/tour',
    '../../models/pay'
], function (Modal, Model, Pay) {
    return Modal.extend({

        //template: _.template($('#tour\\.pay\\.modal\\.tpl').html()),

        events: {
            'click #tour-view #tourCancel'      : 'cancel',
            'click #pay'                        : 'pay'
        },

        initialize: function () {
            Modal.prototype.initialize.call(this, 'Pay');
        },

        render: function (id) {
            var parent = this;
            if(id && _.isString(id)) {
                this.model = new Model();
                this.model.set({id:id}).fetch({success: function () {
                    parent.render();
                }});
                return this;
            }
            Modal.prototype.render.call(this);
        },

        pay: function (e) {
            var fields = {},
                parent = this;
            this.$el.find('form input').each(function (i, item) {
                fields[$(item).attr('name')] = $(item).val();
            });
            var pay = new Pay();
            if( pay.set(fields).validate() ) {
                this.model.set({ paid: true}).save(null, {
                    success: function () {
                        parent.cancel();
                    }
                });
            }
        },

        cancel: function (e) {
            this.$el.modal('hide');
            App.Router.public.setRoute('profile');
        }

    })
});
