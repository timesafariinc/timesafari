define([
    '../modal',
    '../../models/tour',
    '../../models/era',
    '../../models/kind',
    '../../models/message',
    '../../collections/reviews'
], function (Modal, Model, Era, Kind, Message, Reviews) {
    return Modal.extend({

        currentPage: 'tour/view|tour/approved|tour/paid|tour/client|tour/passed|tour/new|tour/pass',

        template: {
            tour    : {
                client      : _.template($('#tour\\.view\\.modal\\.client\\.tpl').html()),
                new         : _.template($('#tour\\.view\\.modal\\.new\\.tpl').html()),
                approved    : _.template($('#tour\\.view\\.modal\\.approved\\.tpl').html()),
                pass        : _.template($('#tour\\.view\\.modal\\.pass\\.tpl').html()),
                passed      : _.template($('#tour\\.view\\.modal\\.passed\\.tpl').html()),
                paid        : _.template($('#tour\\.view\\.modal\\.paid\\.tpl').html())
            }
        },

        events: {
            'click #tour-view .cancel-icon'    : 'cancel',
            'click #tour-view #tourCancel'     : 'cancel',
            'click #apply'          : 'apply',
            'click #tourApprove'    : 'approve',
            'click #tourRefuse'     : 'refuse',
            'click #tourRepeat'     : 'tourRepeat',
            'click #startDialog'    : 'startDialog',
            'click #extend'         : 'extendPayDate',
            'click .send-pass'      : 'pass',
            'click .delete-message' : 'deleteMessage',
            'click .tour-delete'    : 'deleteTour',
            'click .tour-back'      : 'tourBack',
            'click .tour-finish'    : 'tourFinish',
            'click .rate-star'      : 'openReview',
            'click #sendReview'     : 'saveReview'
        },

        blockCancel: false,

        pressChange: false,

        render: function (section, status, id) {
            var parent = this;
            if(section) this.section = section;
            if(status) this.status = status;
            var model = this.model = new Model();
            if(status == 'passed') {
                this.message = new Message();
                this.message.set({id:id}).fetch({
                    success: function (message) {
                        parent.sender = message.get('sender');
                        var id = message.get('content').match(/#.* /);
                        if(id) {
                            model.set({id: id[0].replace(/[# ]/g, '')}).fetch({
                                success: _.bind(function (model) {
                                    model.set(_.pick(message.toJSON(), 'content', 'sender'));
                                    parent.renderData(model);
                                }, parent),
                                error: function (err) {
                                    parent.model
                                        .set(_.pick(message.toJSON(), 'content', 'sender'))
                                        .set({kind: ''});
                                    parent.renderData(parent.model);
                                }
                            });
                        }
                    }
                });
                return this;
            }
            if(!parent.emptyTour)
                model.set({id: id}).fetch({success: _.bind(this.renderData, this)});
            return this;
        },
        
        renderData: function (model) {
            var template = this.template[this.section][this.status];
            if(template) {
                if(!model.has('kind')) model.set({kind: ''});
                if(!model.has('price')) model.set({price: ''});
                var merge = {};
                if(this.section == 'tour') {
                    merge['status'] = {name: model.getStatus(), color: model.getStatusColor()};
                    merge['danger'] = {name: model.getDanger(), color: model.getDanger('color')};
                }
                if(this['status'] == 'passed') {
                    merge['contain'] = this.message.get('content').replace(/[\\\n]/g, '<br>');
                }
                this.$el.find('.modal-body').html(template(_.extend(model.toJSON(), merge)));
                this.$el.find('.rate-star').hover(_.bind(this.pickOutStars, this));
                this.$el.modal();
                this.kind = new Kind();
                this.kind.set({id: model.get('kind')}).fetch({success: _.bind(this.setKind, this)});
                this.reviews = new Reviews();
                this.reviews.fetch({data: {tour: model.get('id')}, success: _.bind(this.setReview, this)});
            }
        },

        setKind: function (model) {
            var era = model.get('era');
            if(era && _.isObject(era))
                this.$el.find('.era').html(era['name']);
            this.$el.find('.kind').text(model.get('name'));
        },

        setReview: function (collection) {
            if(!collection.isEmpty()) {
                var review = this.review = collection.first();
                this.selectedRate(review.get('rate') - 1, this.$el.find('.rate-star'));
                this.$el.find('#review-block textarea').val(review.get('message'));
                this.$el.find('#review-block button').data('id', review.get('id'));
            }
        },

        pass: function (e) {
            var parent = this;
            var client = this.model.get('client');
            var content = 'Ваша заявка: ' + this.model.get("name")
                + ' отклонена.\n' + this.$el.find('textarea').val();
            var message = new Message();
            this.model.set({status: 3}).save(null, {
                success: function () {
                    $.ajax({
                        url: 'message/add/',
                        type: 'POST',
                        data: {receiver: client['id'], content: content}
                    }).done(function (response) {
                        parent.cancel();
                    });
                }
            });
        },

        apply: function (e) {
            var client = this.model.get('client')['id'];
            var assigned_personal = App.Profile.get('id');
            this.model
                .set({status: 1, client: client, assigned_personal: assigned_personal})
                .save({_method: 'PUT'}, {success: _.bind(this.cancel, this)});
        },

        tourFinish: function (e) {
            var client = this.model.get('client')['id'];
            var assigned_personal = App.Profile.get('id');
            this.model
                .set({status: 2, client: client, assigned_personal: assigned_personal})
                .save({_method: 'PUT'}, {success: _.bind(this.cancel, this)});
        },

        pickOutStars: function (e) {
            if(!$(e.target).parent().hasClass('selected')) {
                this.selectedRate($(e.target).index(), $(e.target).parent().children('span'));
            }
        },

        openReview: function (e) {
            this.selectedRate($(e.target).index(), $(e.target).closest('.rate').children('span'));
            $(e.target).closest('.rate').addClass('selected');
            var rate = $(e.target).index() + 1;
            this.$el.find('#review-block').show(300);
            this.$el.find('[name="rate"]').val(rate);
        },

        selectedRate: function (index, $stars) {
            $stars.slice(0, index + 1).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
            $stars.slice(index + 1).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        },

        saveReview: function (e) {
            var $block = $(e.target).closest('#review-block');
            review = {
                tour: this.model.get('id'),
                client: App.Profile.get('id'),
                rate: parseFloat($block.find('input').val()),
                message: $block.find('textarea').val()
            };
            var callback = {
                success: _.bind(this.reviewSaved, this),
                error: _.bind(this.showError, this)
            };
            if(!this.review)
                this.review = this.reviews.create(review, callback);
            else
                this.review.save(review, callback);
        },

        reviewSaved: function (model) {
            var $message = $('<label/>').addClass('col-md-12 text-center text-success').text('Ваш отзыв добавлен');
            this.$el.find('#review-block').append($message);
            setTimeout(_.bind(function () {
                $message.remove();
                this.$el.find('#review-block').hide(200);
            }, this), 1000);
        },

        deleteMessage: function (e) {
            var parent = this;
            this.message.destroy({
                success: function () {
                    parent.cancel();
                }
            })
        },

        deleteTour: function (e) {
            var parent = this;
            this.model.destroy({
                success: function () {
                    parent.cancel();
                }
            });
        },

        tourBack: function (e) {
            App.Router.public.prevRouter();
        },
        
        tourRepeat: function (e) {
            var parent = this;
            var era = this.model.get('era');
            if(_.isObject(era)) era = era['id'];
            var kind = this.model.get('kind');
            if(_.isObject(kind)) kind = kind['id'];
            new Era({id: era}).fetch({
                success: function (modelEra) {
                    new Kind({id: kind}).fetch({
                        success: function (model) {
                            if(modelEra.get('id') != kind) {
                                parent.showError('Повторить тур невозможно, в данной эре нет такого вида животного');
                                return;
                            }
                            if(model.get('count') > 0) {
                                App.Router['public']
                                    .setRoute('tour/create/?era=' + era + '&kind=' + kind + '&repeat=true');
                            } else
                                parent.showError('Нет животных на данный тур');
                        },
                        error: _.bind(parent.showError, parent)
                    })
                },
                error: _.bind(parent.showError, parent)
            });
        },

        startDialog: function (e) {
            var id = $(e.target).data('id');
            setTimeout(function () {
                $('#chat-' + id).trigger('click');
            }, 300);
        },

        extendPayDate: function (e) {
            this.model.extendPayDate({
                success: _.bind(function (res) {
                    $(e.target).parent().prev().text('до ' + res['pay_date'])
                }, this)
            });
        },

        showError: function (err) {
            if(_.isEmpty(err))  return;
            if(_.isObject(err)) err = _.values(err)[0];
            if(_.isArray(err))  err = err[0];
            var errorMsg = '<div class="alert alert-danger error-message text-center" role="alert">' + err + '</div>';
            this.$el.find('.error-message').remove();
            this.$el.find('.row:first').before(errorMsg);
        },

        cancel: function (e) {
            if(this.checkCurrentPage(this.currentPage)) {
                this.$el.modal('hide');
                if(this.isTable) {
                    this.isTemplate = false;
                    App.Router.public.setRoute('tours');
                }
                else
                    App.Router.public.setRoute('profile');
            }
        }

    })
});
