# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-12-15 20:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tour', '0010_auto_20161207_0108'),
    ]

    operations = [
        migrations.AddField(
            model_name='tour',
            name='document_link',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
    ]
