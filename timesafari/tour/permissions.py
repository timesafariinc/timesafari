from rest_framework import permissions
from .models import Tour, Template

class TourCreatePermission(permissions.BasePermission):
    def has_permission(self, request, view):
    	if not request.user.is_authenticated:
    		return False
    	return 'tour.add_tour' in request.user.get_all_permissions()
	    #return request.user.has_perm('tour.add_tour')

class TourUpdatePermission(permissions.BasePermission):
	def has_permission(self, request, view):
		if not request.user.is_authenticated:
			return False
		return 'tour.change_tour' in request.user.get_all_permissions()
 
class TourDeletePermission(permissions.BasePermission):
	def has_permission(self, request, view):
		if not request.user.is_authenticated:
			return False
		return 'tour.delete_tour' in request.user.get_all_permissions()

class TourExtendPermission(permissions.BasePermission):
	def has_permission(self, request, view):
		if not request.user.is_authenticated:
			return False
		if 'tour.change_tour' in request.user.get_all_permissions():
			return True
		tour = Tour.objects.get(pk=view.kwargs.get('pk'))
		if tour.client == request.user:
			return True
		return False



class TemplateCreatePermission(permissions.BasePermission):
    def has_permission(self, request, view):
    	if not request.user.is_authenticated:
    		return False
    	return 'tour.add_template' in request.user.get_all_permissions()

class TemplateUpdatePermission(permissions.BasePermission):
	def has_permission(self, request, view):
		if not request.user.is_authenticated:
			return False
		return 'tour.change_template' in request.user.get_all_permissions()
 
class TemplateDeletePermission(permissions.BasePermission):
	def has_permission(self, request, view):
		if not request.user.is_authenticated:
			return False
		return 'tour.delete_template' in request.user.get_all_permissions()

