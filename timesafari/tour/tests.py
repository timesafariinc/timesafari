from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from rest_framework.authtoken.models import Token
from timesafari.user.models import TimeSafariUser
from timesafari.era.models import Era
from timesafari.kind.models import Kind
from .models import Tour
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import Group

from django.core.files.images import ImageFile
import tempfile
from django.test.utils import override_settings

def get_test_image_file():
    file = tempfile.NamedTemporaryFile(suffix='.jpg')
    image = ImageFile(file, name=file.name)
    return image

MEDIA_ROOT = tempfile.mkdtemp()

@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class CreateTourTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		TimeSafariUser.objects.create_user('consultant@consultant.com', 'consultant', 'consultant', 'consultant')
		consultant = TimeSafariUser.objects.get(email='consultant@consultant.com')
		consultant.groups.add(Group.objects.get(name='Consultant'))
		self.consultant_id = consultant.id

		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		for i in range(5):
			Era.objects.create(name='era ' + str(i + 1), price=i + 1)
		for i in range(10):
			Kind.objects.create(name='kind ' + str(i + 1),
				price=i + 1,
				count=i + 1,
				era=Era.objects.get(id=i // 2 + 1),
				danger=i % 4)

		self.url = reverse('tour-create')

	def test_create_tour_without_login(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'danger': 0, 'duration': 10}
		response = self.guest.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_create_tour_with_client(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'danger': 0, 'duration': 10}
		response = self.client.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Tour.objects.all()), 1)
		tour = Tour.objects.get(id=1)
		self.assertEqual(tour.name, 'era 1 #1')

	def test_create_tour_with_empty_price(self):
		data = {'name': 'new tour', 'era': 1, 'kind': 3, 'danger': 0, 'duration': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Tour.objects.all()), 1)
		tour = Tour.objects.get(id=1)
		self.assertEqual(tour.name, 'era 1 #1')
		self.assertEqual(tour.price, 0)

	def test_create_tour_with_negative_price(self):
		data = {'price': -300, 'era': 1, 'kind': 3, 'duration': 10, 'danger': 2}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_non_int_price(self):
		data = {'price': 'hello', 'era': 1, 'kind': 3, 'duration': 10, 'danger': 2}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_non_int_price(self):
		data = {'price': 123.4, 'era': 1, 'kind': 3, 'duration': 10, 'danger': 2}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_empty_danger(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'duration': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Tour.objects.all()), 1)
		tour = Tour.objects.get(id=1)
		self.assertEqual(tour.name, 'era 1 #1')
		self.assertEqual(tour.danger, 0)

	def test_create_tour_with_negative_danger(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'duration': 10, 'danger': -2}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_non_int_danger(self):
		data = {'price': 'hello', 'era': 1, 'kind': 3, 'duration': 10, 'danger': 'hi'}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_none_exist_danger(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'duration': 10, 'danger': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_empty_era(self):
		data = {'price': 300, 'kind': 3, 'danger': 0, 'duration': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_none_exist_era(self):
		data = {'price': 300, 'kind': 3, 'danger': 0, 'duration': 10, 'era': 100}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_empty_kind(self):
		data = {'price': 300, 'era': 1, 'danger': 0, 'duration': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_non_exist_kind(self):
		data = {'price': 300, 'era': 1, 'danger': 0, 'duration': 10, 'kind': -4}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_empty_duration(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'danger': 2}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Tour.objects.all()), 1)
		tour = Tour.objects.get(id=1)
		self.assertEqual(tour.name, 'era 1 #1')
		self.assertEqual(tour.duration, 0)

	def test_create_tour_with_negative_duration(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'danger': 2, 'duration': -5}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Tour.objects.all()), 0)

	def test_create_tour_with_description(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'danger': 2, 'duration': 5, 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.'}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Tour.objects.all()), 1)
		tour = Tour.objects.get(id=1)
		self.assertEqual(tour.name, 'era 1 #1')

@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class GetTourListTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		TimeSafariUser.objects.create_user('consultant@consultant.com', 'consultant', 'consultant', 'consultant')
		consultant = TimeSafariUser.objects.get(email='consultant@consultant.com')
		consultant.groups.add(Group.objects.get(name='Consultant'))
		self.consultant_id = consultant.id

		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		for i in range(5):
			Era.objects.create(name='era ' + str(i + 1), price=i + 1)
		for i in range(10):
			Kind.objects.create(name='kind ' + str(i + 1),
				price=i + 1,
				count=i + 1,
				era=Era.objects.get(id=i // 2 + 1),
				danger=i % 4)

		for i in range(10):
			Tour.objects.create(
				price = i + 1,
				danger = i % 4,
				era = Era.objects.get(id = (i % 5) + 1),
				kind = Kind.objects.get(id = (i % 10) + 1),
				client = TimeSafariUser.objects.get(id = 1),
				duration = 10
			)
		self.url = reverse('tour-list')


	def test_get_all_tours_without_login(self):
		response = self.guest.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 10)		

	def test_get_all_tours_with_client(self):
		response = self.client.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 10)

	def test_get_all_tours_with_admin(self):
		response = self.admin.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 10)

@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class GetTourDetailTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		TimeSafariUser.objects.create_user('consultant@consultant.com', 'consultant', 'consultant', 'consultant')
		consultant = TimeSafariUser.objects.get(email='consultant@consultant.com')
		consultant.groups.add(Group.objects.get(name='Consultant'))
		self.consultant_id = consultant.id

		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		for i in range(5):
			Era.objects.create(name='era ' + str(i + 1), price=i + 1)
		for i in range(10):
			Kind.objects.create(name='kind ' + str(i + 1),
				price=i + 1,
				count=i + 1,
				era=Era.objects.get(id=i // 2 + 1),
				danger=i % 4)

		for i in range(10):
			Tour.objects.create(
				price = i + 1,
				danger = i % 4,
				era = Era.objects.get(id = (i % 5) + 1),
				kind = Kind.objects.get(id = (i % 10) + 1),
				client = TimeSafariUser.objects.get(id = 1),
				duration = 10
			)
		self.url = reverse('tour-detail', args = [3])

	def test_get_tour_without_login(self):
		response = self.guest.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(response.data['id'], 3)

	def test_get_kind_with_client(self):
		response = self.client.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(response.data['id'], 3)

	def test_get_kind_with_admin(self):
		response = self.admin.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(response.data['id'], 3)

	def test_get_none_exist_kind(self):
		url = reverse('tour-detail', args = [200])
		response = self.admin.get(url)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)



@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class EditTourTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		TimeSafariUser.objects.create_user('consultant@consultant.com', 'consultant', 'consultant', 'consultant')
		consultant = TimeSafariUser.objects.get(email='consultant@consultant.com')
		consultant.groups.add(Group.objects.get(name='Consultant'))
		self.consultant_id = consultant.id

		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		for i in range(5):
			Era.objects.create(name='era ' + str(i + 1), price=i + 1)
		for i in range(10):
			Kind.objects.create(name='kind ' + str(i + 1),
				price=i + 1,
				count=i + 1,
				era=Era.objects.get(id=i // 2 + 1),
				danger=i % 4)

		for i in range(10):
			Tour.objects.create(
				name = 'tour ' + str(i),
				price = i + 1,
				danger = i % 4,
				era = Era.objects.get(id = (i % 5) + 1),
				kind = Kind.objects.get(id = (i % 10) + 1),
				client = TimeSafariUser.objects.get(id = 1),
				duration = 10
			)
		self.url = reverse('tour-edit', args = [3])
		
		self.data = {'price': 300, 'era': 1, 'kind': 3, 'danger': 2, 'duration': 5}

	def test_edit_tour_without_login(self):
		response = self.guest.put(self.url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_edit_tour_with_client(self):
		response = self.client.put(self.url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

	def test_edit_tour_with_admin(self):
		response = self.admin.put(self.url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		tour = Tour.objects.get(id=3)
		self.assertEqual(tour.duration, 5)

	def test_edit_tour_with_empty_name(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'danger': 0, 'duration': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		tour = Tour.objects.get(id=3)
		self.assertEqual(tour.duration, 10)

	def test_edit_tour_with_empty_price(self):
		data = {'era': 1, 'kind': 3, 'danger': 0, 'duration': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		tour = Tour.objects.get(id=3)
		self.assertEqual(tour.duration, 10)

	def test_edit_tour_with_negative_price(self):
		data = {'price': -300, 'era': 1, 'kind': 3, 'duration': 10, 'danger': 2}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_non_int_price(self):
		data = {'price': 'hello', 'era': 1, 'kind': 3, 'duration': 10, 'danger': 2}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_float_price(self):
		data = {'price': 123.4, 'era': 1, 'kind': 3, 'duration': 10, 'danger': 2}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_empty_danger(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'duration': 5}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		tour = Tour.objects.get(id=3)
		self.assertEqual(tour.duration, 5)

	def test_edit_tour_with_negative_danger(self):
		data = {'name': 'new tour', 'price': 300, 'era': 1, 'kind': 3, 'duration': 10, 'danger': -2}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_non_int_danger(self):
		data = {'price': 'hello', 'era': 1, 'kind': 3, 'duration': 10, 'danger': 'hi'}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_none_exist_danger(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'duration': 10, 'danger': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_empty_era(self):
		data = {'price': 300, 'kind': 3, 'danger': 0, 'duration': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_none_exist_era(self):
		data = {'price': 300, 'kind': 3, 'danger': 0, 'duration': 10, 'era': 100}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_empty_kind(self):
		data = {'price': 300, 'era': 1, 'danger': 0, 'duration': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_non_exist_kind(self):
		data = {'price': 300, 'era': 1, 'danger': 0, 'duration': 10, 'kind': -4}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_tour_with_empty_duration(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'danger': 2}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(Tour.objects.all()), 10)
		tour = Tour.objects.get(id=3)
		self.assertEqual(tour.duration, 10)

	def test_edit_tour_with_negative_duration(self):
		data = {'price': 300, 'era': 1, 'kind': 3, 'danger': 2, 'duration': -5}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_none_exist_tour(self):
		url = reverse('tour-edit', args = [200])
		response = self.admin.put(url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class DeleteTourTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		TimeSafariUser.objects.create_user('consultant@consultant.com', 'consultant', 'consultant', 'consultant')
		consultant = TimeSafariUser.objects.get(email='consultant@consultant.com')
		consultant.groups.add(Group.objects.get(name='Consultant'))
		self.consultant_id = consultant.id

		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		for i in range(5):
			Era.objects.create(name='era ' + str(i + 1), price=i + 1)
		for i in range(10):
			Kind.objects.create(name='kind ' + str(i + 1),
				price=i + 1,
				count=i + 1,
				era=Era.objects.get(id=i // 2 + 1),
				danger=i % 4)

		for i in range(10):
			Tour.objects.create(
				price = i + 1,
				danger = i % 4,
				era = Era.objects.get(id = (i % 5) + 1),
				kind = Kind.objects.get(id = (i % 10) + 1),
				client = TimeSafariUser.objects.get(id = 1),
				duration = 10
			)
		self.url = reverse('tour-delete', args = [3])


	def test_delete_tour_without_login(self):
		response = self.guest.delete(self.url)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
		try:
			tour = Tour.objects.get(id=3)
		except ObjectDoesNotExist:
			assert False
		else:
			self.assertEqual(tour.price, 3)

	def test_delete_tour_with_client(self):
		response = self.client.delete(self.url)
		self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
		try:
			tour = Tour.objects.get(id=3)
		except ObjectDoesNotExist:
			assert False
		else:
			self.assertEqual(tour.price, 3)

	def test_delete_tour_with_admin(self):
		response = self.admin.delete(self.url)
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
		try:
			tour = Tour.objects.get(id=3)
		except ObjectDoesNotExist:
			pass
		else:
			assert False, "Object was not deleted!"

	def test_delete_none_exist_tour(self):
		url = reverse('tour-delete', args = [200])
		response = self.admin.delete(url)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
		