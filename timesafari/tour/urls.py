from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
	# /tour/
	url(r'^$', views.TourListView.as_view(), name='tour-list'),
	# /tour/get
	url(r'^get/$', views.TourListView.as_view(), name='tour-list'),
	# /tour/<pk>/
	url(r'^(?P<pk>[0-9]+)/$', views.TourRetrieveView.as_view(), name='tour-detail'),
	# /tour/get/<pk>/
	url(r'^get/(?P<pk>[0-9]+)/$', views.TourRetrieveView.as_view(), name='tour-detail'),
	# /tour/add/
	url(r'^add/$', views.TourCreateView.as_view(), name='tour-create'),
	# /tour/edit/<pk>/
	url(r'^edit/(?P<pk>[0-9]+)/$', views.TourUpdateView.as_view(), name='tour-edit'),
	# /tour/delete/<pk>/
	url(r'^delete/(?P<pk>[0-9]+)/$', views.TourDeleteView.as_view(), name='tour-delete'),
	# /tour/extend/<pk>/
	url(r'^extend/(?P<pk>[0-9]+)/$', views.TourExtendView.as_view(), name='tour-extend'),
]

urlpatterns = format_suffix_patterns(urlpatterns)