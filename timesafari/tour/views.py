from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from .models import Tour, Template
from .serializers import TourSerializer, TourUpdateSerializer, TourCreateSerializer, TourExtendSerializer, TemplateCreateSerializer, TemplateUpdateSerializer, TemplateSerializer 
from rest_framework import permissions
from .permissions import TourCreatePermission, TourUpdatePermission, TourDeletePermission, TourExtendPermission, TemplateCreatePermission, TemplateUpdatePermission, TemplateDeletePermission
from timesafari.user.models import TimeSafariUser
from timesafari.era.models import Era
from django.db.models import F
from timesafari.exceptions import DeletionError

from timesafari.kind.models import Kind

class TourCreateView(generics.CreateAPIView):
	serializer_class = TourCreateSerializer
	permission_classes = (TourCreatePermission, )

	def perform_create(self, serializer):
		template = self.request.POST.get('template', None)
		if template:
			temp = Template.objects.get(pk=template)
			if temp:
				kind = temp.kind
				kind.count = F('count') + 1
				kind.save()
				temp.delete()
		consultant = TimeSafariUser.objects.filter(groups__name='Consultant').order_by('?')[0]
		serializer.save(client=self.request.user, assigned_personal=consultant)

class TourListView(generics.ListAPIView):
	serializer_class = TourSerializer
	permission_classes = (permissions.AllowAny, )

	def get_queryset(self):
		queryset = Tour.objects.all()
		request_status = self.request.query_params.get('status', None)
		if request_status is not None:
			queryset = queryset.filter(status=request_status)
		request_client = self.request.query_params.get('client', None)
		if request_client is not None:
			queryset = queryset.filter(client__id=request_client)
		request_assigned_personal = self.request.query_params.get('assigned_personal', None)
		if request_assigned_personal is not None:
			queryset = queryset.filter(assigned_personal__id=request_assigned_personal)
		request_paid = self.request.query_params.get('paid', None)
		if request_paid is not None:
			queryset = queryset.filter(paid=request_paid)
		return queryset

class TourRetrieveView(generics.RetrieveAPIView):
	serializer_class = TourSerializer
	queryset = Tour.objects.all()
	permission_classes = (permissions.AllowAny, )

class TourUpdateView(generics.UpdateAPIView):
	serializer_class = TourUpdateSerializer
	permission_classes = (TourUpdatePermission, )
	queryset = Tour.objects.all()

class TourDeleteView(generics.DestroyAPIView):
	serializer_class = TourSerializer
	permission_classes = (TourDeletePermission, )
	queryset = Tour.objects.all()

	def perform_destroy(self, instance):
		if instance.status == 0 or instance.status == 1:
			kind = instance.kind
			kind.count = F('count') + 1
			kind.save()
		instance.delete()

class TourExtendView(generics.UpdateAPIView):
	serializer_class = TourExtendSerializer
	permission_classes = (TourExtendPermission, )
	queryset = Tour.objects.all()



class TemplateCreateView(generics.CreateAPIView):
	serializer_class = TemplateCreateSerializer
	permission_classes = (TemplateCreatePermission, )

class TemplateListView(generics.ListAPIView):
	serializer_class = TemplateSerializer
	permission_classes = (permissions.AllowAny, )

	def get_queryset(self):
		queryset = Template.objects.all()
		return queryset

class TemplateRetrieveView(generics.RetrieveAPIView):
	serializer_class = TemplateSerializer
	queryset = Template.objects.all()
	permission_classes = (permissions.AllowAny, )

class TemplateUpdateView(generics.UpdateAPIView):
	serializer_class = TemplateUpdateSerializer
	permission_classes = (TemplateUpdatePermission, )
	queryset = Template.objects.all()

class TemplateDeleteView(generics.DestroyAPIView):
	serializer_class = TemplateSerializer
	permission_classes = (TemplateDeletePermission, )
	queryset = Template.objects.all()

	def perform_destroy(self, instance):
		kind = instance.kind
		kind.count = F('count') + 1
		kind.save()
		instance.delete()
