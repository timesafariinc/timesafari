"""timesafari URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from djoser import views as djoser_views
from . import views as core_views
from .user import views as user_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #index
    url(r'^$', core_views.index, name = 'index'),
    url(r'^index/$', core_views.index, name = 'index'),

    # url for authentication
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #url(r'^auth/', include('djoser.urls')),
    url(r'^registration/$', djoser_views.RegistrationView.as_view(), name='user-registration'),
    url(r'^login/$', user_views.TimeSafariLoginView.as_view(), name='user-login'),
    url(r'^logout/$', djoser_views.LogoutView.as_view(), name='user-logout'),
    url(r'^profile/', include('timesafari.user.urls_profile')),
    url(r'^user/', include('timesafari.user.urls_user')),

    #url for tour
    # /tour/
    url(r'^tour/', include('timesafari.tour.urls')),

    # url for template
    # /template
    url(r'^template/', include('timesafari.tour.urls_template')),

    # url for era
    # /era/
    url(r'^era/', include('timesafari.era.urls')),

    # url for kind
    # /era/
    url(r'^kind/', include('timesafari.kind.urls')),

    # url for message
    # /message/
    url(r'^message/', include('timesafari.message.urls')),

    # url for review
    url(r'^review/', include('timesafari.review.urls')),

    # url for chat
    url(r'^chat/', include('timesafari.chat.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
