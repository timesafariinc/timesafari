from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
from djoser import views as djoser_views


urlpatterns = [
    url(r'^$', djoser_views.UserView.as_view(), name='user-profile'),
        url(r'^edit/$', views.UserUpdateView.as_view(), name='edit-profile'),
    url(r'^edit/email/$', djoser_views.SetUsernameView.as_view(), name='set-username'),
    url(r'^edit/password/$', djoser_views.SetPasswordView.as_view(), name='set-password'),
]
