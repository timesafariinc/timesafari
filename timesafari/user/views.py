from django.db.models import Q
from django.shortcuts import render, redirect
from rest_framework import generics
from django.contrib.auth import authenticate, login
from .serializers import UserSerializer, UserUpdateSerializer, UserAdminSerializer, UserAdminChangeGroupSerializer, UserDisplayInfoSerializer, ConsultantUpdateSerializer
from .models import TimeSafariUser
from .permissions import ConsultantUpdatePermission
from rest_framework import permissions, views, status
from rest_framework.response import Response

from djoser import views as djoser_views
from djoser import serializers, utils

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

class TimeSafariLoginView(djoser_views.LoginView):
	def action(self, serializer):
		token = utils.login_user(self.request, serializer.user)
		token_serializer_class = serializers.serializers_manager.get('token')
		login(self.request, serializer.user)
		return Response(
			data=token_serializer_class(token).data,
			status=status.HTTP_200_OK,
		)

class UserUpdateView(views.APIView):
	permission_classes = (permissions.IsAuthenticated, )

	def put(self, request, format=None):
		user = TimeSafariUser.objects.get(id=self.request.user.id)
		serializer = UserUpdateSerializer(user, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_200_OK, )
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST, )

class UserListView(generics.ListAPIView):
	permission_classes = (permissions.IsAuthenticated, )
	serializer_class = UserDisplayInfoSerializer

	def get_queryset(self):
		queryset = TimeSafariUser.objects.all()
		request_group = self.request.query_params.get('group', None)
		if request_group is not None:
			if request_group.isdigit():
				queryset = queryset.filter(Q(groups__id=int(request_group)))
			else:
				queryset = queryset.filter(Q(groups__name=request_group))
		return queryset

class UserDetailView(generics.RetrieveAPIView):
	permission_classes = (permissions.IsAuthenticated, )
	serializer_class = UserAdminSerializer
	queryset = TimeSafariUser.objects.all()

class UserChangeGroupView(views.APIView):
	permission_classes = (permissions.IsAdminUser, )

	def get_object(self, pk):
		try:
			return TimeSafariUser.objects.get(pk=pk)
		except TimeSafariUser.DoesNotExist:
			raise Http404

	def put(self, request, pk, format=None):
		user = self.get_object(pk)
		serializer = UserAdminChangeGroupSerializer(user, data=request.data)
		if serializer.is_valid():
			groups = request.query_params.get('groups', None)
			if groups is not None:
				serializer.save(is_admin = (groups[0] == '1'))
			else:
				serializer.save(is_admin = user.is_admin)				
			return Response(serializer.data, status=status.HTTP_200_OK, )
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST, )

class ConsultantUpdateView(generics.UpdateAPIView):
	permission_classes = (ConsultantUpdatePermission, )
	serializer_class = ConsultantUpdateSerializer
	queryset = TimeSafariUser.objects.all()

class UserDeleteView(generics.DestroyAPIView):
	permission_classes = (permissions.IsAdminUser, )
	serializer_class = UserAdminSerializer
	queryset = TimeSafariUser.objects.all()

"""
def registration(request):
	form = RegistrationForm(request.POST or None)
	if form.is_valid():
		user = form.save(commit = False)
		email = form.cleaned_data['email']
		password = form.cleaned_data['password']
		user.set_password(password)
		user.save()
		user = authenticate(email = email, password = password)

		if user is not None:
			if user.is_active:
				login(request, user)
				return redirect('index')
	context = {'form': form}
	return render(request, 'personal_create.html', context)

def login_user(request):
	form = LoginForm(request.POST or None)
	if request.method == "POST":
		email = request.POST['email']
		password = request.POST['password']
		user = authenticate(email = email, password = password)
		if user is not None:
			if user.is_active:
				login(request, user)
				return redirect('index')
			else:
				return render(request, 'login.html', {'form': form, 'error_message': 'Your account is not available.'})
		return 	render(request, 'login.html', {'form': form, 'error_message': 'Incorrect email or password.'})	
	return render(request, 'login.html', {'form': form})
"""